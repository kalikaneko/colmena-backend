# Contributing

First off, thanks for taking the time to contribute!

Remember that this is open source software so please consider the other people who will read your code.
Make it look nice for them, document your logic in comments and add or update the unit test cases.

This library is used by various other projects, companies and individuals in live production environments so please discuss any breaking changes with us before making them.

Feel free to join us in TODO(santi): add official comms channel.

## Merge Requests

[Fork the repo on Gitlab](https://git.colmena.network/maia/backend-colmena) and clone it to your local machine.

```bash
git clone https://git.colmena.network/YOUR_USERNAME/colmena-backend && cd colmena-backend
```

Here is a guide on [how to configure a remote repository](https://docs.github.com/en/free-pro-team@latest/github/collaborating-with-issues-and-pull-requests/configuring-a-remote-for-a-fork).

Check out a new branch, make changes, run tests, commit & sign-off, then push branch to your fork.

```bash
$ git checkout -b <BRANCH_NAME>
# edit files
$ make style vet test
$ git add <CHANGED_FILES>
$ git commit -s
$ git push <FORK> <BRANCH_NAME>
```

Open a [new merge request](https://github.com/foo/bar/compare) in the main `maia/colmena-backend` repository.
Please describe the purpose of your PR and remember link it to any related issues.

*We may ask you to rebase your feature branch or squash the commits in order to keep the history clean.*

## Development Guides

* Keep the history linear (clean rebases and no merge commits).
* Use informative commit messages. Use tags: for commit line, and reference the
  issue you're closing or addressing (e.g.: -Resolves:#1, -Related:#2,
  -Closes:#3).
* Document your logic in code comments.
* Add tests for bug fixes and new features.
* Use UNIX-style (LF) line endings.
* End every file with a single blank line.
* Use the UTF-8 character set.
* If you're a maintainer, do not merge directly any functional change that is
  not code-reviewed. We will send ninjas to your home.
* Follow [django best practices](https://django-best-practices.readthedocs.io/en/latest/)
