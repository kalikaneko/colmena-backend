#!/bin/sh

SETTINGS=colmena.settings.prod
BIN=python

create_db() {
  set -e

  echo ======== Create DB ========
  $BIN ./bin/postgres.py CREATE || true
  echo
}

create_superadmin() {
  set -e
  local superadmin_email=$1
  local superadmin_password=$2
  local nc_username=$3
  local nc_password=$4

  echo ======== Create Superadmin ========
  $BIN manage.py create_superadmin \
    $superadmin_email \
    $superadmin_password \
    $nc_username \
    $nc_password
  echo
}

migrate() {
  set -e

  echo ======== Starting ecto migration ========
  $BIN manage.py makemigrations --settings=$SETTINGS
  $BIN manage.py migrate --settings=$SETTINGS
  echo
}

local_seeds() {
  set -e

  echo ======== Creating local seeds ========
  $BIN manage.py maybeloaddata apps/sites/seeds/00-sites.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/01-permissions.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/02-groups.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/04-languages.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/05-regions.json --settings=$SETTINGS
  echo
}

e2e_seeds() {
  set -e

  echo ======== Creating e2e seeds ========
  $BIN manage.py maybeloaddata apps/sites/seeds/00-e2e-sites.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/01-permissions.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/02-groups.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/04-languages.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/05-regions.json --settings=$SETTINGS
  echo
}

prod_seeds() {
  set -e

  echo ======== Creating prod seeds ========
  $BIN manage.py maybeloaddata apps/sites/seeds/00-sites.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/01-permissions.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/02-groups.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/04-languages.json --settings=$SETTINGS
  $BIN manage.py loaddata apps/accounts/seeds/05-regions.json --settings=$SETTINGS
  echo
}

setup_db() {
  set -e

  # Run the create db
  create_db
  # Run the migrate script
  migrate
}

setup_static() {
  set -e

  echo "======== Collecting static files ========"
  $bin ./manage.py collectstatic --noinput

  echo "======== Compiling translations ========"
  $bin ./manage.py compilemessages -l en -l es -i venv
}

start_e2e() {
  set -e

  local workers=$GUNICORN_WORKERS

  echo
  echo "Starting e2e instance"
  echo
  echo Using settings=$SETTINGS
  echo

  # Setup static files
  setup_static

  # Run the setup_db script
  setup_db

  # Run seeds creation
  e2e_seeds

  # Create Superadmin User
  create_superadmin \
    $SUPERADMIN_EMAIL \
    $SUPERADMIN_PASSWORD \
    $NEXTCLOUD_ADMIN_USER \
    $NEXTCLOUD_ADMIN_PASSWORD

  echo ======== Starting Nginx ========
  service nginx start
  echo ======== Starting Local colmena ========
  gunicorn --workers $workers --bind unix:/opt/app/app.sock -m 777 colmena.wsgi:application
}

start_local() {
  set -e

  local workers=$GUNICORN_WORKERS

  echo
  echo "Starting local instance"
  echo
  echo Using settings=$SETTINGS
  echo

  # Setup static files
  setup_static

  # Run the setup_db script
  setup_db

  # Run seeds creation
  local_seeds

  # Create Superadmin User
  create_superadmin \
    $SUPERADMIN_EMAIL \
    $SUPERADMIN_PASSWORD \
    $NEXTCLOUD_ADMIN_USER \
    $NEXTCLOUD_ADMIN_PASSWORD

  echo ======== Starting Nginx ========
  service nginx start
  echo

  echo ======== Starting Local colmena ========
  gunicorn --workers $workers --bind unix:/opt/app/app.sock -m 777 colmena.wsgi:application
}

start_prod() {
  set -e

  local workers=$GUNICORN_WORKERS

  echo
  echo "Starting prod instance"
  echo
  echo Using settings=$SETTINGS
  echo

  # Setup static files
  setup_static
  # Run the setup_db script
  setup_db
  # Run seeds creation
  prod_seeds

  # Create Superadmin User
  create_superadmin \
    $SUPERADMIN_EMAIL \
    $SUPERADMIN_PASSWORD \
    $NEXTCLOUD_ADMIN_USER \
    $NEXTCLOUD_ADMIN_PASSWORD

  echo ======== Starting colmena ========

  gunicorn --workers $workers colmena.wsgi:application --bind 0.0.0.0:$PORT
}

case $1 in
  create_db) "$@"; exit;;
  migrate) "$@"; exit;;
  e2e_seeds) "$@"; exit;;
  local_seeds) "$@"; exit;;
  prod_seeds) "$@"; exit;;
  setup_db) "$@"; exit;;
  start_e2e) "$@"; exit;;
  start_local) "$@"; exit;;
  start_prod) "$@"; exit;;
esac
