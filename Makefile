ENV_FILE ?= .env
CONTAINER_NAME ?= colmena_backend
IMAGE_NAME ?= colmena_backend
COMPOSE_DIR ?= devops/local/
DOCKERFILE_DIR ?= devops/builder/
SHELL := /bin/bash
VENV := venv
BIN := $(VENV)/bin

GREEN=\033[0;32m
YELLOW=\033[0;33m
NOFORMAT=\033[0m

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif

default: help

.PHONY: help
#❓ help: @ Displays this message
help: SHELL := /bin/sh
help:
	@echo ""
	@echo "List of available MAKE targets for development usage."
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Examples:"
	@echo ""
	@echo "	make ${GREEN}venv${NOFORMAT}		- Setup an initial virtual env"
	@echo "	make ${GREEN}setup${NOFORMAT}		- Setup requirements, db and third party apps"
	@echo "	make ${GREEN}server${NOFORMAT}		- Start server"
	@echo ""
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(firstword $(MAKEFILE_LIST))| tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "${GREEN}%-30s${NOFORMAT} %s\n", $$1, $$2}'
	@echo ""

#💻 lint: @ Runs the black code formatter
.PHONY: lint
lint:
	$(BIN)/black --fast .

#💻 db.create: @ Create postgres db
db.create: db.create.dev db.create.test

db.create.dev:
	$(PYTHON) ./bin/postgres.py CREATE || true

db.create.test: SETTINGS_MODULE = colmena.settings.test
db.create.test:
	$(PYTHON) manage.py reset_db --settings=${SETTINGS_MODULE} --no-input

#💻 db.drop: @ Delete sqlite db
db.drop: db.drop.dev db.drop.test

db.drop.dev: SETTINGS_MODULE = colmena.settings.dev
db.drop.dev:
	$(PYTHON) manage.py reset_db --settings=${SETTINGS_MODULE} --no-input
	$(PYTHON) ./bin/postgres.py DROP || true

db.drop.test: SETTINGS_MODULE = colmena.settings.test
db.drop.test:
	$(PYTHON) manage.py reset_db --settings=${SETTINGS_MODULE} --no-input

#💻 db.migrate: @ Make and run migrations
db.migrate: db.migrate.dev db.migrate.test

db.migrate.dev: SETTINGS_MODULE = colmena.settings.dev
db.migrate.dev:
	$(PYTHON) ./manage.py makemigrations --settings=${SETTINGS_MODULE}
	$(PYTHON) ./manage.py migrate --settings=${SETTINGS_MODULE}

db.migrate.test: SETTINGS_MODULE = colmena.settings.test
db.migrate.test:
	$(PYTHON) ./manage.py makemigrations --settings=${SETTINGS_MODULE}
	$(PYTHON) ./manage.py migrate --settings=${SETTINGS_MODULE}

#💻 db.seeds: @ Run seeds
db.seeds: SETTINGS_MODULE = colmena.settings.dev
db.seeds:
	for i in $$(find . -wholename "*/seeds/*.json"  -printf "%f\n" | sort -t '\0' -n); do \
	    echo "Loading $$i" ;\
		find -name $$i -exec $(PYTHON) manage.py loaddata {} --settings=${SETTINGS_MODULE} \; ;\
	done

#💻 db.reset: @ Access the Postgres Docker database interactively with psql
db.reset: db.drop db.create db.migrate db.seeds

#🐳 devops.up: @ Starts the server from docker
devops.up:
	@cd ${COMPOSE_DIR} && docker compose up api -d

#🐳 devops.down: @ Stops the api service
devops.down:
	@cd ${COMPOSE_DIR} && docker compose down api

#🐳 devops.build: @ Builds a new image for the backend service.
docker.build: SUPERADMIN_EMAIL:=$(SUPERADMIN_EMAIL)
docker.build: SUPERADMIN_PASSWORD:=$(SUPERADMIN_PASSWORD)
docker.build: NEXTCLOUD_ADMIN_USER:=$(NEXTCLOUD_ADMIN_USER)
docker.build: NEXTCLOUD_ADMIN_PASSWORD:=$(NEXTCLOUD_ADMIN_PASSWORD)
docker.build:
	@docker build ./ \
		-f $(DOCKERFILE_DIR)/Dockerfile.local \
		-t colmena_backend

#🐳 docker.connect: @ Connect to the colmena_backend running container
docker.connect:
	@docker exec -it colmena_backend /bin/bash

#🐳 docker.delete: @ Delete the colmena_backend docker container
docker.delete: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.delete:
	@docker rm $(CONTAINER_NAME) 2> /dev/null || true

#🐳 docker.logs: @ Show logs for the docker container
docker.logs: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs:
	@docker logs $(CONTAINER_NAME)

#🐳 docker.logs.watch: @ Watch logs for the docker container
docker.logs.watch: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs.watch:
	@docker logs $(CONTAINER_NAME) -f

#🐳 docker.release: @ Re-create a docker image and run it
docker.release: docker.stop docker.delete docker.build docker.run

#🐳 docker.rerun: @ Stops and deletes old container to re-run a fresh new container
docker.rerun: docker.stop docker.delete docker.run

#🐳 docker.run: @ Run the colmena_backend docker instance
docker.run: PORT:=5001
docker.run: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.run: IMAGE_NAME:=$(IMAGE_NAME)
docker.run:
	@docker run \
		--detach \
		--name $(CONTAINER_NAME) \
		--network local_colmena_devops \
		-p 5001:5001 \
		--env PORT=5001 \
		--env-file .env.prod \
		$(IMAGE_NAME)

#🐳 docker.stop: @ Stop the colmena_backend docker container
docker.stop: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.stop:
	@docker container stop $(CONTAINER_NAME) 2> /dev/null || true

#📦 gen.openapi.schema: @ Generates openapi schemas
gen.openapi.schema: FORMAT=openapi-json
gen.openapi.schema:
	$(PYTHON) ./manage.py spectacular --color --file schema.json --format $(FORMAT)


#💻 server: @ Starts the Django server
server:
	$(PYTHON) ./manage.py runserver

#💻 server_plus: @ Starts the Django server plus
server_plus:
	$(PYTHON) ./manage.py runserver_plus --nopin

#📦 setup: @ Install requirements and setup the database,t ranslations and openapi clients.
setup: install setup.openapi db.create db.migrate db.seeds translations.compile setup.superadmin

.PHONY: install
#📦 install: @ Install python requirements for dev and test envs.
install: install.dev install.test

install.dev : REQUIREMENTS_ENV = dev
install.dev:
	@$(BIN)/pip3 install -r requirements/${REQUIREMENTS_ENV}.txt

install.test : REQUIREMENTS_ENV = test
install.test:
	@$(BIN)/pip3 install -r requirements/${REQUIREMENTS_ENV}.txt

setup.openapi: API_WRAPPER_PATH = apps/nextcloud/openapi
setup.openapi:
	@$(BIN)/openapi-python-generator \
		$(API_WRAPPER_PATH)/schema.json \
		$(API_WRAPPER_PATH)/client

setup.superadmin: NEXTCLOUD_ADMIN_USER:=$(NEXTCLOUD_ADMIN_USER)
setup.superadmin: NEXTCLOUD_ADMIN_PASSWORD:=$(NEXTCLOUD_ADMIN_PASSWORD)
setup.superadmin:
	@$(PYTHON) ./manage.py create_superadmin \
		$(SUPERADMIN_EMAIL) \
		$(SUPERADMIN_PASSWORD) \
		$(NEXTCLOUD_ADMIN_USER) \
		$(NEXTCLOUD_ADMIN_PASSWORD)

#💻 shell: @ Starts the shell plus
shell: IPYTHONDIR := $(VENV)/.ipython
shell:
	$(PYTHON) manage.py shell_plus

#⚙️  shell.setup: @   Create and configures an ipython profile for the shell plus
shell.setup: PROFILE_DIR := $(VENV)/.ipython/profile_default
shell.setup: IPYTHON_CONFIG := $(PROFILE_DIR)/ipython_config.py
shell.setup:
	@rm $(IPYTHON_CONFIG) || true
	@$(BIN)/ipython profile create --profile-dir $(PROFILE_DIR)
	@echo "c.InteractiveShellApp.exec_lines = ['%autoreload 2']; c.InteractiveShellApp.extensions = ['autoreload']" >> $(IPYTHON_CONFIG)

#🧪 test: @ Run backend test suites
.PHONY: test
test: SETTINGS_MODULE = colmena.settings.test
test:
	$(PYTHON) ./manage.py test --verbosity=0 --parallel --failfast --exclude-tag=skip --exclude-tag=ldap --settings=${SETTINGS_MODULE}

#🧪 test.all: @ Run all test suites
test.all: SETTINGS_MODULE = colmena.settings.test
test.all:
	$(PYTHON) ./manage.py test --verbosity=0 --parallel --failfast --exclude-tag=skip --settings=${SETTINGS_MODULE}

#🧪 test.harvest: @ Run BDD tests w/ aloe
test.harvest: SETTINGS_MODULE = colmena.settings.test--settings=colmena.settings.test
test.harvest:
	$(PYTHON) ./manage.py harvest --settings=${SETTINGS_MODULE}

#🧪 test.nextcloud: @ Run nextcloud integration tests
test.nextcloud: SETTINGS_MODULE = colmena.settings.test
test.nextcloud:
	$(PYTHON) ./manage.py test --verbosity=0 --parallel --failfast --tag=nextcloud --settings=${SETTINGS_MODULE}

#🧪 test.watch: @ Run all tests and watch for changes
test.watch: 
test.watch:
	./bin/tests.sh watch_all

#🧪 test.watch.tag: @ Run tests with a certain tag and watch for changes. Default TAG=wip
test.watch.tag: 
test.watch.tag: TAG:=wip
test.watch.tag:
	./bin/tests.sh watch_tag $(TAG)

#🧪 test.wip: @ Run backend test suites tagged with wip
test.wip: SETTINGS_MODULE = colmena.settings.test
test.wip:
	$(PYTHON) ./manage.py test --verbosity=0 --parallel --failfast --tag=wip --settings=${SETTINGS_MODULE}

#📙 translations: @ Extract new untranslated text and merge translations to avaialble languages.
translations:
	@$(PYTHON) ./manage.py makemessages -l en -l es -i venv -e py,html -d django

#📦 translations.compile: @ Compiles extracted translations into .mo files
translations.compile:
	@$(PYTHON) ./manage.py compilemessages -l en -l es -i venv

#⚙️ venv: @  Create and a virtual env
.PHONY: venv
venv:
	python -m venv $(VENV)

#⚙️  venv.activate: @  Make active venv environment
venv.activate:
	source $(VENV)/bin/activate
