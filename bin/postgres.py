import os
import psycopg2

import sys

action = str.upper(sys.argv[1])

db_name = os.environ.get("POSTGRES_DATABASE")

# Establishing the connection
conn = psycopg2.connect(
    user=os.environ.get("POSTGRES_USERNAME"),
    password=os.environ.get("POSTGRES_PASSWORD"),
    host=os.environ.get("POSTGRES_HOSTNAME"),
    port=os.environ.get("POSTGRES_PORT"),
)
conn.autocommit = True

# Creating a cursor object using the cursor() method
cursor = conn.cursor()

# Preparing query to create a database
sql = f"""{action} database {db_name}"""

# Creating a database
cursor.execute(sql)
print("Success.")

# Closing the connection
conn.close()
