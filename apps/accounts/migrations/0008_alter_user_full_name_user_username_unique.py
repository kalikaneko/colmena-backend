# Generated by Django 4.2.3 on 2023-09-18 15:44

import colmena.validators.validators
from django.db import migrations, models
import django.db.models.functions.text


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0007_user_nc_app_password"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="full_name",
            field=models.CharField(
                default="",
                max_length=80,
                validators=[
                    colmena.validators.validators.ValidatorOnlyLettersAndSpaces()
                ],
                verbose_name="users_prop_full_name",
            ),
        ),
        migrations.AddConstraint(
            model_name="user",
            constraint=models.UniqueConstraint(
                django.db.models.functions.text.Lower("username"),
                name="username_unique_value",
                violation_error_message="The username already exists",
            ),
        ),
    ]
