import random
from apps.accounts.models import Region


VALID_ATTRS = {"name": "some name"}

UPDATE_ATTRS = {"name": "some updated name"}

INVALID_ATTRS = {
    "name": None,
}


def valid_attrs(attrs={}):
    return (
        VALID_ATTRS
        | {
            "name": unique_value(VALID_ATTRS["name"]),
        }
        | attrs
    )


def update_attrs(attrs={}):
    return (
        UPDATE_ATTRS
        | {
            "name": unique_value(UPDATE_ATTRS["name"]),
        }
        | attrs
    )


def invalid_attrs(attrs={}):
    return INVALID_ATTRS | attrs


def create(attrs={}):
    return Region.objects.create(**valid_attrs(attrs))


def unique_value(value):
    return f"{str(random.randrange(1,100))}-{value}"
