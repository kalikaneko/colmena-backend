from django.urls import reverse_lazy
from rest_framework import generics, viewsets, views, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from django.views.generic.edit import UpdateView
from django.contrib.auth import login
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.models import Site
from django.http import Http404, HttpResponseRedirect
from django.utils.translation import gettext_lazy
from django.shortcuts import get_object_or_404, render
from django.db.models import Q
from django.core.exceptions import ValidationError

from apps.accounts.common import is_superadmin_or_staff
from apps.accounts.models import (
    User,
    UserInvitation,
    Language,
    OrganizationMemberInvitation,
    get_super_admin,
)
from apps.organizations.models import (
    UserOrganizationGroupProxy,
    Organization,
    Team,
    UserTeam,
)
from apps.organizations.resources import team as team_manager
from apps.accounts.serializers import (
    GroupSerializer,
    LanguageSerializer,
    PasswordResetConfirmSerializer,
    PasswordResetSerializer,
    UserSerializer,
    ConfirmInvitationSerializer,
    OrganizationMemberInvitationRequestSerializer,
)
from apps.accounts.form import PasswordResetForm, UserInvitationForm
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.resources import files as nextcloud_files_manager
from dj_rest_auth.views import (
    PasswordResetView as _PasswordResetView,
    PasswordResetConfirmView as _PasswordResetConfirmView,
)
from django.contrib.auth.views import (
    PasswordResetView as CorePasswordResetView,
    PasswordResetConfirmView as CorePasswordResetConfirmView,
)

from colmena.serializers.serializers import ErrorSerializer
from colmena.settings.base import FRONTEND_INVITATION_SITE_NAME

from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample

# -----------------------------------------------------------------------------
# Create your views here.
#


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Group.objects.filter(Q(name="User") | Q(name="Admin"))
    serializer_class = GroupSerializer


class LanguageViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A Language viewset
    """

    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class UserViewSet(viewsets.ModelViewSet, generics.UpdateAPIView):
    queryset = User.objects.all().order_by("-created_at")
    serializer_class = UserSerializer
    filter_fields = ("username",)
    http_method_names = ["get", "patch"]

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        if not User.objects.filter(pk=pk).exists():
            return Response(ErrorSerializer("ERRORS_USER_NOT_FOUND").data, status=404)
        return super(UserViewSet, self).retrieve(request, pk=pk)

    def update(self, request, *args, **kwargs):
        user = User.objects.get(email=request.user.email)
        self.object = self.get_object()
        if user.id != self.object.id:
            return Response(
                ErrorSerializer("ERRORS_INVALID_OPERATION").data, status=401
            )
        # Extract data to request
        data = request.data

        # Check if request.data is valid
        if (
            not (set(data.keys()).issubset(set(UserSerializer().get_fields().keys())))
            or not data.keys()
        ):
            return Response(
                ErrorSerializer("ERRORS_INVALID_REQUEST_DATA").data, status=400
            )

        # Remove email to data if same to email user request
        if data.get("email") == request.user.email:
            data.pop("email")

        # use the partial flag only to validate the given fields.
        serializer = self.get_serializer(data=request.data, partial=True)

        if serializer.is_valid():
            serializer.update(user, serializer.validated_data)
            return Response(UserSerializer(user).data, status=200)
        else:
            return Response(serializer.errors, status=400)


class UserInvitationUpdateView(UpdateView):
    model = UserInvitation
    success_url = "/admin/"
    form_class = UserInvitationForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def get(self, request, *args, **kwargs):
        token = kwargs["token"]
        pk_user = kwargs["pk"]
        user = UserInvitation.objects.get(pk=pk_user)
        if not PasswordResetTokenGenerator().check_token(user, token):
            raise Http404(gettext_lazy("Your URL may have expired."))
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        # HINT: The login function is used to authenticate the user and redirect
        # to the initial screen with the user already logged in.
        login(self.request, self.get_object())
        return response


class ConfirmInvitationView(views.APIView):
    # FIXME: Find another way create a 'open' endpoint,for now only can do a POST requests
    authentication_classes = []
    permission_classes = [AllowAny]
    serializer_class = ConfirmInvitationSerializer
    queryset = User.objects.all()

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="id",
                description="The user's primary key",
                location=OpenApiParameter.PATH,
                type=int,
                required=True,
                examples=[
                    OpenApiExample("User with pk 2", value=2),
                    OpenApiExample("User with pk 17", value=17),
                ],
            ),
            OpenApiParameter(
                name="token",
                description="The user token generate for backend",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "User Token ", value="bwqchr-7bac39b82a7f81404dabc863e0c22f1c"
                    ),
                    OpenApiExample(
                        "User Token", value="bwqcka-4844346fcfe79787906b799c1c927305/"
                    ),
                ],
            ),
        ],
    )
    def post(self, request, *args, **kwargs):
        token = kwargs["token"]
        pk_user = kwargs["pk"]

        # HINT:Raise 404 if user pk not exist
        user = get_object_or_404(User, pk=pk_user)
        # HINT:Raise 401 if user pk belongs to a another group
        if user.belongs_to_group("Superadmin") or user.belongs_to_group("Staff"):
            return Response(
                ErrorSerializer("ERRORS_INVALID_OPERATION").data, status=401
            )
        if not PasswordResetTokenGenerator().check_token(user, token):
            return Response(
                ErrorSerializer("ERRORS_INVALID_INVITATION_TOKEN").data, status=400
            )
        try:
            organization = UserOrganizationGroupProxy.objects.get(
                user=user
            ).organization
        except UserOrganizationGroupProxy.DoesNotExist:
            return Response(
                ErrorSerializer("ERRORS_USER_ORGANIZATION_NOT_FOUND").data, status=400
            )
        data = request.data
        serializer = self.serializer_class(
            data=data, queryset=User.objects.all().exclude(pk=pk_user)
        )

        if serializer.is_valid():
            # Hint: Use the information validated in the serializer
            new_username = serializer.validated_data["username"]
            new_password = serializer.validated_data["password"]
            superadmin_user = get_super_admin()

            # TODO: create user in nextcloud: use the app password of a
            # privileged account
            nextcloud_user_manager.create_user(
                user_id=new_username,
                display_name=user.full_name,
                user_email=user.email,
                password=new_password,
                auth_user_id=superadmin_user.get_nextcloud_user_id(),
                auth_user_app_password=superadmin_user.nc_app_password,
            )

            # Checks that the user was created successfully
            if nextcloud_user_manager.user_exists_in_nextcloud(
                new_username,
                superadmin_user.get_nextcloud_user_id(),
                superadmin_user.nc_app_password,
            ):
                # Create app password for the new user
                nc_app_password = nextcloud_user_manager.create_app_password(
                    new_username, new_password
                )
                if nc_app_password is None:
                    nextcloud_user_manager.delete_user(
                        user_id=new_username,
                        auth_user_id=superadmin_user.get_nextcloud_user_id(),
                        auth_user_app_password=superadmin_user.nc_app_password,
                    )
                    raise ValidationError(
                        ErrorSerializer(
                            "ERRORS_NEXTCLOUD_APP_PASSWORD_CREATION_FAILED"
                        ).data
                    )
            else:
                raise ValidationError(
                    ErrorSerializer("ERRORS_NEXTCLOUD_USER_NOT_FOUND").data
                )

            # Set the app password to the django User entity.
            user.nc_app_password = nc_app_password
            group_name = nextcloud_group_manager.build_group_name(
                org_id=organization.id,
                team_identifier=nextcloud_group_manager.GENERAL_GROUP_NAME,
            )
            # Use new_username because the user was don't save yet.
            nextcloud_user_id = new_username
            organization_owner = organization.get_organization_owner_user()
            # Create a talk folder
            nextcloud_files_manager.create_talk_folder(user)

            if user.belongs_to_group("OrgOwner"):
                try:
                    team_manager.create(
                        group_name, nextcloud_user_id, organization, user
                    )
                except Exception as e:
                    raise ValidationError(
                        ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_CREATION_FAILED").data
                    )
            else:
                try:
                    team = Team.objects.get(nc_group_id=group_name)
                    team_manager.add_member(
                        user,
                        nextcloud_user_id,
                        team,
                        organization_owner,
                        superadmin_user,
                    )
                except Exception as e:
                    nextcloud_user_manager.delete_user(
                        user_id=new_username,
                        auth_user_id=superadmin_user.get_nextcloud_user_id(),
                        auth_user_app_password=superadmin_user.nc_app_password,
                    )
                    raise ValidationError(
                        ErrorSerializer(
                            "ERRORS_NEXTCLOUD_ADD_USER_TO_GROUP_FAILED"
                        ).data
                    )
            team_manager.create_personal_workspace(user, nextcloud_user_id)
            user.is_active = True
            serializer.update(user, data)
            mailing_client = MailingClient()
            context = {
                "user": user,
                "support_web": "https://colmena.dw.com/support",
                "support_team": "Colmena Team",
                "confirmation_link": Site.objects.get(
                    name=FRONTEND_INVITATION_SITE_NAME
                ).domain,
                "recovery_link": "https://colmena.dw.com/recovery",
            }

            email_template = mailing_client.get_template_by_language(
                "admin-confirmation", user.language.first().iso_code
            )

            email = mailing_client.build(
                get_noreply_from(),
                [user.email],
                email_template,
                context,
            )

            email.send()
        else:
            return Response(serializer.errors, status=400)

        return Response(UserSerializer(user).data, status=201)


class OrganizationMemberViewSet(generics.GenericAPIView):
    serializer_class = OrganizationMemberInvitationRequestSerializer
    permission_classes = [IsAuthenticated]

    @extend_schema(request=OrganizationMemberInvitationRequestSerializer())
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            organization_id = request.data.get("organization_id")
            organization = Organization.objects.get(id=organization_id)
            if not UserOrganizationGroupProxy.objects.filter(
                user=request.user,
                organization=organization_id,
                group=Group.objects.get(name="OrgOwner"),
            ).exists():
                return Response(
                    ErrorSerializer("ERRORS_INVALID_PERMISSION").data, status=401
                )

            members = []
            for data_user in serializer.data["new_users"]:
                member = OrganizationMemberInvitation.objects.create(
                    # FIXME: Extract left part of email for use to username
                    username=data_user["email"].split("@")[0],
                    full_name=data_user["full_name"],
                    email=data_user["email"],
                )
                members.append(member)
            for member in members:
                user_group = Group.objects.get(id=data_user["group_id"])
                member.language.add(organization.language)
                member.groups.add(user_group)
                UserOrganizationGroupProxy.objects.create(
                    user=member,
                    organization=organization,
                    group=user_group,
                )
            return Response("ORGANIZATION_MEMBER_INVITATION_CREATED", status=201)
        else:
            return Response(ErrorSerializer("ERRORS_INVALID_REQUEST").data, status=400)


class PasswordResetView(_PasswordResetView):
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                "PASSWORD_RESET_REQUEST_RECEIVED",
                status=status.HTTP_200_OK,
            )
        else:
            error_message = serializer.errors["email"][0]
            return Response(error_message, status=status.HTTP_200_OK)


class PasswordResetConfirmView(_PasswordResetConfirmView):
    serializer_class = PasswordResetConfirmSerializer
    permission_classes = (AllowAny,)

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                "PASSWORD_RESET_CONFIRMATION_SUCCESS",
                status=status.HTTP_200_OK,
            )
        else:
            error_message = serializer.errors["non_field_errors"][0]
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)


class AdminPasswordResetView(CorePasswordResetView):
    permission_classes = (AllowAny,)
    form_class = PasswordResetForm
    token_generator = PasswordResetTokenGenerator
    template_name = "accounts/passwordreset_form.html"
    title = gettext_lazy("Password Reset")

    def renderPasswordResetDone(self, request, context):
        return render(
            request,
            "accounts/passwordreset_done.html",
            context,
            status=status.HTTP_200_OK,
        )

    def form_valid(self, form):
        request = self.request
        context = self.get_context_data(form=form)
        try:
            form.save()
            # builds context for displaying email
            context["email_address"] = form.cleaned_data.get("email")
        except User.DoesNotExist:
            context["email_address"] = form.data["email"]
        return self.renderPasswordResetDone(request, context)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        return self.form_valid(form)


class AdminPasswordResetConfirmView(CorePasswordResetConfirmView):
    permission_classes = (AllowAny,)
    post_reset_login = False
    post_reset_login_backend = None
    success_url = None
    template_name = "accounts/passwordresetconfirm_form.html"
    title = gettext_lazy("Password Reset")
    token_generator = PasswordResetTokenGenerator()

    def dispatch(self, request, *args, **kwargs):
        self.user = self.get_user(kwargs["uid"])
        if not is_superadmin_or_staff(self.user):
            raise Http404(gettext_lazy("Invalid Token."))
        if self.user is not None and self.token_generator.check_token(
            self.user, kwargs["token"]
        ):
            self.validlink = True
            return self.post(request, *args, **kwargs)
        else:
            raise Http404(gettext_lazy("Your URL may have expired."))

    def form_valid(self, form):
        form.save()
        return render(
            self.request,
            "accounts/passwordresetconfirm_complete.html",
            context=None,
            status=status.HTTP_200_OK,
        )
