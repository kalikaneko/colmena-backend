from django.contrib.auth.models import Group
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.models import Site
from rest_framework import exceptions
from rest_framework import serializers
from apps.accounts.common import is_superadmin_or_staff
from apps.accounts.models import (
    User,
    UserInvitation,
    Language,
)
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from apps.organizations.models import Organization, UserOrganizationGroupProxy
from rest_framework.validators import UniqueValidator
from apps.sites.common import build_site_with_token
from colmena.serializers.serializers import ErrorSerializer, GroupSerializer
from apps.organizations.serializers import OrganizationSerializer
from dj_rest_auth.serializers import PasswordResetSerializer as _PasswordResetSerializer
from drf_spectacular.utils import extend_schema_field


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    organization = serializers.SerializerMethodField()
    id = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
            "groups",
            "organization",
            "full_name",
            "created_at",
            "updated_at",
        )
        extra_kwargs = {
            "email": {"error_messages": {"invalid": "ERRORS_EMAIL_INVALID_FORMAT"}},
            "username": {"error_messages": {"max_length": "ERRORS_FULLNAME_LENGTH"}},
        }

    @extend_schema_field(field=OrganizationSerializer)
    def get_organization(self, obj):
        organization = UserOrganizationGroupProxy.objects.filter(user=obj.pk)
        if organization.exists():
            return OrganizationSerializer(organization.first().organization).data
        else:
            return None


class ConfirmInvitationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    username = serializers.CharField(max_length=30)

    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop("queryset", User.objects.all())
        super().__init__(*args, **kwargs)

    def validate_username(self, value):
        validator = UniqueValidator(queryset=self.queryset)
        validator(value, self.fields["username"])
        return value

    def update(self, instance, validated_data):
        password = validated_data.get("password", None)
        user = super().update(instance, validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user

    class Meta:
        model = UserInvitation
        fields = (
            "username",
            "password",
        )


class OrganizationMemberInvitationSerializer(serializers.Serializer):
    full_name = serializers.CharField()
    email = serializers.EmailField(validators=[UniqueValidator(User.objects.all())])
    group_id = serializers.IntegerField()

    def validate(self, attrs):
        username = attrs.get("email").split("@")[0]
        if User.objects.filter(username__iexact=username).exists():
            raise serializers.ValidationError("ERRORS_USERNAME_EXISTS")
        return super().validate(attrs)

    def validate_group_id(self, group_id):
        if not isinstance(group_id, int):
            raise serializers.ValidationError("ERRORS_GROUP_ID_IS_NOT_NUMBER")
        elif not Group.objects.filter(id=group_id).exists():
            raise serializers.ValidationError("ERRORS_GROUP_ID_IS_NOT_VALID_GROUP")
        elif not self.is_valid_group_id(group_id):
            raise serializers.ValidationError(
                "ERRORS_GROUP_ID_IS_NOT_ORGANIZATION_GROUP"
            )
        return group_id

    def is_valid_group_id(self, group_id):
        return Group.objects.get(id=group_id).name in ["Admin", "User"]


class OrganizationMemberInvitationRequestSerializer(serializers.Serializer):
    organization_id = serializers.IntegerField()
    new_users = OrganizationMemberInvitationSerializer(many=True)

    def validate_organization_id(self, organization_id):
        if not isinstance(organization_id, int):
            raise serializers.ValidationError("ERRORS_ORGANIZATION_ID_IS_NOT_NUMBER")
        elif not Organization.objects.filter(id=organization_id).exists():
            raise serializers.ValidationError(
                "ERRORS_ORGANIZATION_ID_IS_NOT_VALID_ORGANIZATION"
            )
        return organization_id

    def validate_new_users(self, new_users):
        emails = list(map(lambda usr: usr["email"], new_users))
        filtered_emails = list(dict.fromkeys(emails))
        if len(emails) != len(filtered_emails):
            raise serializers.ValidationError("ERRORS_INVALID_REQUEST")
        return new_users


class PasswordResetSerializer(_PasswordResetSerializer):
    email = serializers.EmailField()

    def validate_email(self, email):
        user_queryset = User.objects.filter(email=email)
        if not user_queryset.exists():
            raise exceptions.ValidationError("ERRORS_USER_NOT_FOUND")
        return super().validate(email)

    def save(self):
        from colmena.settings.base import FRONTEND_PASSWORD_RESET_SITE_NAME

        email_address = self.validated_data["email"]

        user = User.objects.get(email=email_address)
        language = user.language.first()

        if language is None:
            user_language = "en"
        else:
            user_language = language.iso_code

        url_path = Site.objects.get(name=FRONTEND_PASSWORD_RESET_SITE_NAME).domain
        token = PasswordResetTokenGenerator().make_token(user)
        restore_password_link = build_site_with_token(
            url_path, {"pk": user.pk, "token": token}
        )

        mail_client = MailingClient()
        email_template = mail_client.get_template_by_language(
            "user-password-reset", user_language
        )
        context = {
            "user": user,
            "support_team": "Colmena Team",
            "recovery_link": restore_password_link,
        }
        email = mail_client.build(
            get_noreply_from(),
            [email_address],
            email_template,
            context,
        )
        email.send()


class PasswordResetConfirmSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=128)
    password_confirm = serializers.CharField(max_length=128)
    uid = serializers.CharField()
    token = serializers.CharField()

    def password_validation(self, attrs):
        password = attrs["password"]
        password_confirm = attrs["password_confirm"]
        if password and password_confirm and password != password_confirm:
            raise exceptions.ValidationError("ERRORS_PASSWORD_MISMATCH")

    def validate(self, attrs):
        try:
            from django.utils.http import urlsafe_base64_decode as uid_decoder

            uid = int(uid_decoder(attrs["uid"]))
            self.user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise exceptions.ValidationError("ERRORS_INVALID_USER")

        if is_superadmin_or_staff(
            self.user
        ) or not PasswordResetTokenGenerator().check_token(self.user, attrs["token"]):
            raise exceptions.ValidationError("ERRORS_INVALID_TOKEN")

        self.password_validation(attrs)
        return attrs

    def save(self):
        new_password = self.data["password"]
        self.user.set_password(new_password)
        self.user.save()
        return self.user
