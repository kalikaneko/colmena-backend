from apps.accounts.models import (
    Language,
    User,
    UserInvitation,
    OrganizationMemberInvitation,
    get_super_admin,
)
from apps.accounts.support import user_fixture, language_fixture, region_fixture
from apps.organizations.support import organization_fixture
from apps.organizations.models import UserOrganizationGroupProxy, Team, UserTeam
from apps.organizations.organizations import get_personal_workspace
from apps.nextcloud.resources import auth as nextcloud_auth_manager
from apps.nextcloud.resources import users as nextcloud_user_manager
from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.organizations.resources import team as team_manager
from apps.nextcloud.resources import utils as nextcloud_utils

from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.management import call_command
from django.core.exceptions import BadRequest, ValidationError
from django.template.response import TemplateResponse
from django.test import TestCase, Client, tag
from django.urls import reverse

from http import HTTPStatus
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.hashers import make_password


class UserModelTest(TestCase):
    def test_create_user_with_valid_attrs_returns_a_new_user(self):
        # Setup
        valid_attrs = user_fixture.valid_attrs()

        # Exercise
        user = User.objects.create(**valid_attrs)

        # Verify
        self.assertEqual(user.email, valid_attrs["email"])
        self.assertEqual(user.username, valid_attrs["username"])
        self.assertEqual(user.password, valid_attrs["password"])
        self.assertEqual(user.full_name, valid_attrs["full_name"])
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_active)

    def test_create_super_user(self):
        # Setup
        valid_attrs = user_fixture.valid_attrs({"is_superuser": True})

        # Exercise
        super_user = User.objects.create(**valid_attrs)

        # Verify
        self.assertEqual(super_user.email, valid_attrs["email"])
        self.assertEqual(super_user.username, valid_attrs["username"])
        self.assertEqual(super_user.password, valid_attrs["password"])
        self.assertEqual(super_user.full_name, valid_attrs["full_name"])

        self.assertTrue(super_user.is_staff)
        self.assertTrue(super_user.is_superuser)
        self.assertTrue(super_user.is_active)

    def test_user_is_created_with_default_values(self):
        # Setup
        valid_attrs = user_fixture.valid_attrs()

        # Exercise
        user = User.objects.create(
            email=valid_attrs["email"],
            password=valid_attrs["password"],
        )

        # Verify
        self.assertEqual(user.email, valid_attrs["email"])
        self.assertEqual(user.password, valid_attrs["password"])

        self.assertEqual(user.username, "")
        self.assertEqual(user.full_name, "")

        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertFalse(user.is_active)


class UserInvitationModelTest(TestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
    ]

    def setUp(self):
        valid_attrs = user_fixture.valid_attrs()

        region = region_fixture.create()

        UserInvitation.objects.create_user(
            email=valid_attrs["email"],
            username=valid_attrs["username"],
            full_name=valid_attrs["full_name"],
            region=region,
        )

        self.client = Client()
        self.valid_attrs = valid_attrs
        self.superadmin_group = Group.objects.get(name="Superadmin")
        self.staff_group = Group.objects.get(name="Staff")

    def test_user_without_password_cant_login(self):
        # Setup
        update_attrs = user_fixture.update_attrs()

        # Exercise
        login_success = self.client.login(
            username=self.valid_attrs["username"], password=update_attrs["password"]
        )

        # Verify
        self.assertFalse(login_success)

    def test_inactive_user_cant_login(self):
        # Setup
        update_attrs = user_fixture.update_attrs()
        user = UserInvitation.objects.get(username=self.valid_attrs["username"])
        user.set_password(update_attrs["password"])

        # Exercise
        login_success = self.client.login(
            username=self.valid_attrs["username"], password=update_attrs["password"]
        )

        # Verify
        self.assertFalse(login_success)

    def test_active_user_login_using_username(self):
        # Setup
        update_attrs = user_fixture.update_attrs()

        user = UserInvitation.objects.get(username=self.valid_attrs["username"])
        user.set_password(update_attrs["password"])
        user.is_active = True
        user.save()

        # Exercise
        login_success = self.client.login(
            username=self.valid_attrs["username"], password=update_attrs["password"]
        )

        # Verify
        self.assertTrue(login_success)

    def test_staff_user_cannot_access_the_user_invitation_form(self):
        # Setup
        update_attrs = user_fixture.update_attrs()
        user = UserInvitation.objects.get(username=self.valid_attrs["username"])

        user.set_password(update_attrs["password"])

        user.is_staff = True
        user.is_active = True
        user.groups.add(self.staff_group)
        user.save()

        login_success = self.client.login(
            username=self.valid_attrs["username"], password=update_attrs["password"]
        )
        self.assertTrue(login_success)

        # Exercise
        get_response = self.client.get(
            "/admin/accounts/userinvitation/add/", follow=True
        )

        # Verify
        self.assertEqual(get_response.status_code, HTTPStatus.FORBIDDEN)


class UserInvitationUpdateViewTests(TestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        region = region_fixture.create()
        superadmin_attrs = user_fixture.valid_attrs()
        superadmin = UserInvitation.objects.create_user(
            email=superadmin_attrs["email"],
            username=superadmin_attrs["username"],
            full_name=superadmin_attrs["full_name"],
            region=region,
        )

        staff_attrs = user_fixture.valid_attrs(
            {"email": "staff_user@email.net", "username": "staff_username"}
        )
        staff_user = UserInvitation.objects.create_user(
            email=staff_attrs["email"],
            username=staff_attrs["username"],
            full_name=staff_attrs["full_name"],
            region=region,
        )

        self.superadmin_attrs = superadmin_attrs
        self.superadmin = superadmin
        self.staff_attrs = staff_attrs
        self.staff_user = staff_user
        self.client = Client()
        self.superadmin_group = Group.objects.get(name="Superadmin")
        self.staff_group = Group.objects.get(name="Staff")
        self.language = Language.objects.get(iso_code="es")

    def test_a_new_superadmin_user_visits_the_user_invitation_form(self):
        # Setup
        superadmin = UserInvitation.objects.get(
            username=self.superadmin_attrs["username"]
        )
        superadmin.set_password(self.superadmin_attrs["password"])
        superadmin.is_staff = self.superadmin_attrs["is_staff"]
        superadmin.is_active = self.superadmin_attrs["is_active"]
        superadmin.groups.add(self.superadmin_group)
        superadmin.save()

        login_success = self.client.login(
            username=self.superadmin_attrs["username"],
            password=self.superadmin_attrs["password"],
        )
        self.assertTrue(login_success)

        # Exercise
        response = self.client.get("/admin/accounts/userinvitation/add/", follow=True)

        # Verify
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "User invitations")
        self.assertIsInstance(response, TemplateResponse)

    def test_a_staff_user_cannot_visit_the_user_invitation_form(self):
        # Setup
        user = UserInvitation.objects.get(username=self.staff_attrs["username"])
        user.set_password(self.staff_attrs["password"])
        user.is_staff = self.staff_attrs["is_staff"]
        user.is_active = self.staff_attrs["is_active"]
        user.groups.add(self.staff_group)
        user.save()

        login_success = self.client.login(
            username=self.staff_attrs["username"], password=self.staff_attrs["password"]
        )
        self.assertTrue(login_success)

        # Exercise
        response = self.client.get("/admin/accounts/userinvitation/add/", follow=True)

        # Verify
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_a_staff_user_cant_invite_users(self):
        # Setup
        user = UserInvitation.objects.get(username=self.staff_attrs["username"])
        user.set_password(self.staff_attrs["password"])
        user.is_staff = self.staff_attrs["is_staff"]
        user.is_active = self.staff_attrs["is_active"]
        user.groups.add(self.staff_group)
        user.save()

        login_success = self.client.login(
            username=self.staff_attrs["username"], password=self.staff_attrs["password"]
        )
        self.assertTrue(login_success)

        data = {
            "username": "some new username",
            "full_name": "some new user fullname",
            "email": "123@email.com",
            "groups": [self.staff_group.pk],
            "language": [self.language.pk],
        }

        # Exercise
        response = self.client.post("/admin/accounts/userinvitation/add/", data)

        # Verify
        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class TestConfirmInvitationView(APITestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        self.user = user_fixture.create({"is_superuser": False})

        self.valid_password = "Ad_12da12:?1d"
        self.short_password = "12?ADSasd"
        self.only_alpha_and_digit_password = "Passwordpassword1340"
        self.only_lowercase_and_digit_password = "pass12345__wordpassword"
        self.only_digit_password = "1234567892342983254328"
        self.superadmin_group = Group.objects.get(name="Superadmin")
        self.staff_group = Group.objects.get(name="Staff")
        self.org_owner_group = Group.objects.get(name="OrgOwner")
        self.user.groups.add(self.org_owner_group)
        self.language = language_fixture.create()
        self.user_group = Group.objects.get(name="User")
        self.admin_group = Group.objects.get(name="Admin")
        self.organization = organization_fixture.create({"language": self.language})
        self.user.language.add(self.language)
        UserOrganizationGroupProxy.objects.create(
            user=self.user,
            organization=self.organization,
            group=self.user_group,
        )
        self.nc_password_length = 72

    def test_invitation_fails_on_http_methods_other_than_post(self):
        # Setup
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise + Verify
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.put(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.patch(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.delete(url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    @tag("nextcloud")
    def test_an_invitation_is_sent_when_the_token_is_valid(self):
        # Setup
        group_name = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()

        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertNotEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Teardown
        nextcloud_group_manager.remove_group(
            group_id=group_name,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_an_invitation_fails_when_the_nextcloud_admin_does_not_exist(self):
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()
        # Setup
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.valid_password}

        # Verify
        self.assertEqual(self.user.nc_app_password, "")

        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_a_nextcloud_user_is_created_on_successful_invitation(self):
        # Setup
        group_name = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        self.superadmin = get_super_admin()

        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.valid_password}

        # Verify
        self.assertEqual(self.user.nc_app_password, "")
        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        updated_user = User.objects.get(username=self.user.username)
        nc_user = nextcloud_user_manager.get_user(
            user_id=updated_user,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        nc_group_name = nextcloud_group_manager.get_group(
            group_id=group_name, user=updated_user
        )
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=group_name,
            user=updated_user,
        )
        # Test standard length from app_password created with cli
        self.assertEqual(len(updated_user.nc_app_password), self.nc_password_length)

        self.assertEqual(nc_user["id"], updated_user.username)
        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], updated_user.email)
        self.assertEqual(nc_user["displayname"], updated_user.full_name)
        self.assertEqual(nc_group_name, group_name)
        self.assertEqual(nc_conversation["name"], group_name)

        # Teardown
        nextcloud_group_manager.remove_group(
            group_id=group_name,
            user=self.superadmin,
        )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=self.superadmin.get_nextcloud_user_id(),
            auth_user_app_password=self.superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=self.user.username,
                auth_user_id=self.superadmin.get_nextcloud_user_id(),
                auth_user_app_password=self.superadmin.nc_app_password,
            )

    @tag("nextcloud")
    def test_a_nextcloud_user_is_created_on_successful_invitation_for_an_organization_admin(
        self,
    ):
        # General Setup
        group_name = nextcloud_group_manager.build_group_name(
            org_id=self.organization.id, team_identifier="general"
        )
        user_fixture.create_superadmin()
        superadmin = get_super_admin()

        # Setup OrgOwner user
        UserOrganizationGroupProxy.objects.filter(
            user=self.user,
            organization=self.organization,
            group=self.user_group,
        ).update(group=self.org_owner_group)

        nextcloud_user_manager.create_user(
            user_id=self.user.username,
            display_name=self.user.full_name,
            user_email=self.user.email,
            password=self.valid_password,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        nc_app_password = nextcloud_user_manager.create_app_password(
            self.user.username, self.valid_password
        )
        self.user.nc_app_password = nc_app_password
        self.user.save()
        # Create Group
        nextcloud_group_manager.create_group(group_id=group_name, user=superadmin)
        # Add user in group
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name,
            user_id=self.user.username,
            user=superadmin,
        )
        # Provides conversation creation privileges to the Admin
        nextcloud_group_manager.promote_user_to_subadmin(
            group_id=group_name,
            user_id=self.user.username,
            user=superadmin,
        )
        # Create a conversation group
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            room_name=group_name, group_id=group_name, user=self.user
        )

        # Setup Admin user
        admin_user = user_fixture.create(attrs={"is_superuser": False})
        admin_user.groups.add(self.admin_group)
        admin_user.language.add(self.language)
        admin_user.save()

        # Create Team
        organization_team = Team.objects.create(
            nc_group_id=group_name,
            nc_conversation_token=nextcloud_utils.get_conversation_token(
                nc_conversation
            ),
            organization=self.organization,
        )

        # Setup Admin user in organization
        UserOrganizationGroupProxy.objects.create(
            user=admin_user,
            organization=self.organization,
            group=self.admin_group,
        )
        # Setup post
        token = PasswordResetTokenGenerator().make_token(admin_user)
        url = reverse("invitations", args=[admin_user.pk, token])
        data = {"username": admin_user.username, "password": self.valid_password}

        # Verify
        self.assertEqual(admin_user.nc_app_password, "")
        with self.assertRaises(BadRequest):
            nextcloud_user_manager.get_user(
                user_id=admin_user.username,
                auth_user_id=superadmin.get_nextcloud_user_id(),
                auth_user_app_password=superadmin.nc_app_password,
            )

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        new_user = User.objects.get(username=admin_user.username)
        nc_user = nextcloud_user_manager.get_user(
            user_id=new_user.get_nextcloud_user_id(),
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        nc_group_name = nextcloud_group_manager.get_group(
            group_id=group_name, user=new_user
        )
        group_members = nextcloud_group_manager.get_group_members(
            group_id=group_name, user=new_user
        )
        nc_conversation = nextcloud_talk_manager.search_user_conversation(
            filter_value=group_name,
            user=new_user,
        )
        user_team = UserTeam.objects.get(user=new_user, team=organization_team)
        personal_workspace_team = get_personal_workspace(
            new_user.get_nextcloud_user_id()
        )

        # Test standard length from app_password created with cli
        self.assertEqual(len(new_user.nc_app_password), self.nc_password_length)

        self.assertEqual(nc_user["id"], new_user.username)
        self.assertTrue(nc_user["enabled"])
        self.assertEqual(nc_user["email"], new_user.email)
        self.assertEqual(nc_user["displayname"], new_user.full_name)
        self.assertEqual(nc_group_name, group_name)
        self.assertEqual(nc_conversation["name"], group_name)
        self.assertEqual(len(group_members), 2)
        self.assertTrue(group_members.__contains__(self.user.username))
        self.assertTrue(group_members.__contains__(new_user.username))

        # Test UserTeam
        self.assertIsInstance(user_team, UserTeam)

        # Teardown
        team_manager.remove_from_team(new_user, organization_team, superadmin)
        team_manager.delete_personal_workspace(personal_workspace_team, superadmin)
        team_manager.delete(organization_team, superadmin)
        nextcloud_user_manager.delete_user(
            user_id=new_user.username,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )
        with self.assertRaises(BadRequest) as context:
            nextcloud_user_manager.get_user(
                user_id=new_user.username,
                auth_user_id=superadmin.get_nextcloud_user_id(),
                auth_user_app_password=superadmin.nc_app_password,
            )
        nextcloud_user_manager.delete_user(
            user_id=self.user.username,
            auth_user_id=superadmin.get_nextcloud_user_id(),
            auth_user_app_password=superadmin.nc_app_password,
        )

    def test_fails_when_the_password_is_short(self):
        # Setup
        error_password_is_short = (
            "This password is too short. It must contain at least 12 characters."
        )
        code_password_is_short = "password_too_short"
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.short_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, code_password_is_short)
        self.assertEqual(str(error_list[0]), error_password_is_short)

    def test_fails_when_the_password_lacks_a_special_character(self):
        # Setup
        invalid_password = "This password must contain at least 1 special character."
        invalid_password_coded = "min_length_special_characters"
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {
            "username": self.user.username,
            "password": self.only_alpha_and_digit_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_coded)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_password_lacks_an_uppercase_letter(self):
        # Setup
        invalid_password = "This password must contain at least 1 upper case letter."
        invalid_password_code = "min_length_upper_characters"
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {
            "username": self.user.username,
            "password": self.only_lowercase_and_digit_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_code)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_password_lacks_alphabet_characters(self):
        # Setup
        invalid_password = "This password is entirely numeric."
        invalid_password_code = "password_entirely_numeric"
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.only_digit_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        error_list = response.data["password"]
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(error_list[0].code, invalid_password_code)
        self.assertEqual(str(error_list[0]), invalid_password)

    def test_fails_when_the_username_is_invalid(self):
        # Setup
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {
            "username": user_fixture.invalid_attrs()["username"],
            "password": self.valid_password,
        }

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_when_the_username_already_exists(self):
        # Setup
        another_user = user_fixture.create()
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": another_user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_when_the_user_belongs_to_a_superadmin_group(self):
        # Set up superadmin user
        superadmin_user = self.user
        superadmin_user.groups.add(self.superadmin_group)
        token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[self.user.pk, token])
        data = {"username": self.user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_fails_when_the_user_belongs_to_a_staff_group(self):
        # Set up staff user
        staff_user = user_fixture.create()
        staff_user.groups.add(self.staff_group)
        staff_token = PasswordResetTokenGenerator().make_token(self.user)
        url = reverse("invitations", args=[staff_user.pk, staff_token])
        data = {"username": staff_user.username, "password": self.valid_password}

        # Exercise
        response = self.client.post(url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestUserApiView(APITestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
    ]

    def setUp(self):
        self.valid_attrs = user_fixture.valid_attrs()
        self.language = language_fixture.create()
        self.organization = organization_fixture.create({"language": self.language})
        self.user = user_fixture.create()
        self.user.language.add(self.language)
        self.url = reverse("user-detail", args=[self.user.pk])

    def test_an_authenticated_user_retrieves_its_own_user_details(self):
        # Setup
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(self.url, format="json")
        data = response.data

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get("id"), self.user.id)
        self.assertEqual(data.get("username"), self.user.username)
        self.assertEqual(data.get("email"), self.user.email)
        self.assertEqual(data.get("full_name"), self.user.full_name)
        self.assertEqual(data.get("group"), None)
        self.assertEqual(data.get("organization"), None)

    def test_an_authenticated_user_with_group_and_organization_retrieves_its_own_user_details(
        self,
    ):
        # Setup
        user_group = Group.objects.get(name="User")
        UserOrganizationGroupProxy.objects.create(
            user=self.user,
            organization=self.organization,
            group=user_group,
        )
        self.user.groups.add(user_group)

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(self.url, format="json")
        data = response.data
        response_organization = data.get("organization")

        self.assertEqual(len(data.get("groups")), 1)
        response_group = data.get("groups")[0]

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get("id"), self.user.id)
        self.assertEqual(data.get("username"), self.user.username)
        self.assertEqual(data.get("email"), self.user.email)
        self.assertEqual(data.get("full_name"), self.user.full_name)
        self.assertEqual(response_group.get("name"), self.user.groups.first().name)
        self.assertEqual(response_group.get("id"), self.user.groups.first().id)
        self.assertEqual(response_organization.get("name"), self.organization.name)
        self.assertEqual(
            response_organization.get("email"),
            self.organization.email,
        )
        self.assertEqual(
            response_organization.get("website"),
            self.organization.website,
        )
        self.assertEqual(
            response_organization.get("additional_info"),
            self.organization.additional_info,
        )
        self.assertEqual(
            response_organization.get("country").get("name"),
            self.organization.country.name,
        )
        self.assertEqual(
            response_organization.get("country").get("iso_code"),
            self.organization.country.code,
        )
        self.assertEqual(
            response_organization.get("language").get("id"),
            self.organization.language.id,
        )
        self.assertEqual(
            response_organization.get("language").get("name"),
            self.organization.language.name,
        )
        self.assertEqual(
            response_organization.get("language").get("iso_code"),
            self.organization.language.iso_code,
        )

    def test_an_authenticated_user_retrieves_another_user_details(self):
        # Setup
        user = user_fixture.create()
        self.client.force_authenticate(user=user)

        # Exercise
        response = self.client.get(self.url, format="json")
        data = response.data

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["username"], self.user.username)
        self.assertEqual(data["email"], self.user.email)
        self.assertEqual(data["full_name"], self.user.full_name)

    def test_a_non_authenticated_user_cannot_retrieve_a_user_detail(self):
        # Exercise
        response = self.client.get(self.url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_successfully_updates_a_user_when_attrs_are_valid(self):
        new_username = self.valid_attrs["username"]
        new_email = self.valid_attrs["email"]
        data = {"username": new_username, "email": new_email}

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("username"), new_username)
        self.assertEqual(response.data.get("email"), new_email)

    def test_successfully_update_a_user_discarding_the_email(self):
        # Setup
        new_username = self.valid_attrs["username"]
        new_full_name = self.valid_attrs["full_name"]
        data = {
            "username": new_username,
            "email": self.user.email,
            "full_name": new_full_name,
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("username"), new_username)
        self.assertEqual(response.data.get("full_name"), new_full_name)
        self.assertEqual(response.data.get("email"), self.user.email)

    def test_fails_with_bad_request_when_the_username_is_too_long(self):
        # Setup
        error_long_username = "ERRORS_FULLNAME_LENGTH"
        invalid_attrs = user_fixture.invalid_attrs(
            {"username": "loremipsumloremipsumloremimpsunloremipmsum"}
        )
        data = {"username": invalid_attrs["username"]}
        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["username"][0], error_long_username)

    def test_fails_when_the_email_format_is_not_valid(self):
        # Setup
        error_invalid_email = "ERRORS_EMAIL_INVALID_FORMAT"
        invalid_attrs = user_fixture.invalid_attrs({"email": "lorem"})
        data = {
            "email": invalid_attrs["email"],
        }
        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=data, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(response.data["email"][0], error_invalid_email)

    def test_fails_for_empty_body_or_non_existent_fields(self):
        # Setup
        error_body = "ERRORS_INVALID_REQUEST_DATA"
        empty_data = {}
        fake_data = {"fake_field": "fake_field"}

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        empty_response = self.client.patch(self.url, data=empty_data, format="json")
        fake_response = self.client.patch(self.url, data=fake_data, format="json")

        # Verify
        self.assertEqual(empty_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(fake_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(empty_response.data["error_code"], error_body)
        self.assertEqual(fake_response.data["error_code"], error_body)


class TestLanguagesApiView(APITestCase):
    def setUp(self):
        self.language = language_fixture.create()
        self.user = user_fixture.create()

    def test_a_non_authenticated_user_cannot_retrieve_a_language_list(self):
        # Setup
        url = reverse("language-list")

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_an_authenticated_user_successfully_retrieves_a_language_list(self):
        # Setup
        url = reverse("language-list")

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["id"], self.language.id)
        self.assertEqual(data[0]["name"], self.language.name)
        self.assertEqual(data[0]["iso_code"], self.language.iso_code)

    def test_a_non_authenticated_user_cannot_retrieve_a_language(self):
        # Setup
        url = reverse("language-detail", args=[self.language.pk])

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_an_authenticated_user_successfully_retrieves_a_language(self):
        # Setup
        url = reverse("language-detail", args=[self.language.pk])

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(data["id"], self.language.id)
        self.assertEqual(data["name"], self.language.name)
        self.assertEqual(data["iso_code"], self.language.iso_code)


class TestGroupsApiView(APITestCase):
    fixtures = [
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
    ]

    def setUp(self):
        self.user = user_fixture.create()

    def test_successfully_retrieves_only_organizations_groups(self):
        # Setup
        url = reverse("group-list")

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]["name"], "Admin")
        self.assertEqual(response.data[1]["name"], "User")

    def test_a_non_authenticated_request_cannot_retrieve_groups(self):
        # Setup
        url = reverse("group-list")

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestOrganizationMemberViewSet(APITestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
    ]

    def setUp(self):
        self.language = language_fixture.create()
        self.user = user_fixture.create({"region": region_fixture.create()})
        self.user.language.add(self.language)
        self.organization = organization_fixture.create({"language": self.language})
        self.user_attrs = user_fixture.valid_attrs()
        self.another_user_attrs = user_fixture.valid_attrs()
        self.user_group = Group.objects.get(name="User")
        self.admin_group = Group.objects.get(name="Admin")
        self.org_owner_group = Group.objects.get(name="OrgOwner")
        self.user_admin_organization = UserOrganizationGroupProxy.objects.create(
            user=self.user,
            organization=self.organization,
            group=self.org_owner_group,
        )

        self.organization_member_invitations_url = reverse(
            "organization_member_invitations"
        )

    def build_user_invitations(self, user_attrs, group):
        return {
            "full_name": user_attrs["full_name"],
            "email": user_attrs["email"],
            "group_id": group.id,
        }

    def test_an_authenticated_user_invite_a_new_organization_member(self):
        # Setup
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.admin_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, "ORGANIZATION_MEMBER_INVITATION_CREATED")

    def test_an_authenticated_user_invite_new_organization_members(self):
        # Setup
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.admin_group),
                self.build_user_invitations(self.another_user_attrs, self.user_group),
            ],
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, "ORGANIZATION_MEMBER_INVITATION_CREATED")

    def test_a_non_authenticated_user_cannot_invite_organization_members(self):
        # Setup

        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }
        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_a_non_organization_admin_cannot_invite_users_to_an_organization(self):
        # Setup
        another_user_not_admin_organization = user_fixture.create()
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=another_user_not_admin_organization)
        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_a_organization_user_cannot_invite_users_to_an_organization(self):
        # Setup
        non_org_admin_user = user_fixture.create()
        non_org_admin_user.language.add(self.language)
        UserOrganizationGroupProxy.objects.create(
            user=non_org_admin_user,
            organization=self.organization,
            group=self.user_group,
        )

        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=non_org_admin_user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_an_invitation_fails_when_the_email_is_not_unique(
        self,
    ):
        # Setup
        self.user_attrs["email"] = self.user.email
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"], "ERRORS_INVALID_REQUEST")

    def test_an_invitation_fails_when_the_username_is_not_unique(
        self,
    ):
        # Setup
        self.user_attrs["email"] = f"{self.user.username}@mail.com"
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"], "ERRORS_INVALID_REQUEST")

    def test_an_invitation_fails_when_the_username_already_exists(
        self,
    ):
        # Setup
        self.user_attrs["email"] = f"{self.user.username.upper()}@mail.com"
        data = {
            "organization_id": self.organization.id,
            "new_users": [
                self.build_user_invitations(self.user_attrs, self.user_group)
            ],
        }

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.post(
            self.organization_member_invitations_url, data=data, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"], "ERRORS_INVALID_REQUEST")


class TestLoginView(APITestCase):
    fixtures = [
        "apps/sites/seeds/00-sites.json",
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
    ]

    def setUp(self):
        self.valid_password = user_fixture.valid_attrs()["password"]
        self.language = language_fixture.create()
        self.admin_group = Group.objects.get(name="Admin")
        self.user = user_fixture.create(
            {"password": make_password(self.valid_password)}
        )
        self.organization = organization_fixture.create({"language": self.language})
        self.user.language.add(self.language)
        self.user.groups.add(self.admin_group)
        self.another_user_attrs = user_fixture.update_attrs()
        self.url = reverse("rest_login")

    def test_user_login_successfully_with_email(self):
        # Setup
        data = {"email": self.user.email, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_login_successfully_with_username(self):
        # Setup
        data = {"username": self.user.username, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_fails_when_the_user_belongs_to_the_superadmin_group(self):
        # Setup
        self.user.groups.clear()
        self.user.groups.add(Group.objects.get(name="Superadmin"))
        data = {"username": self.user.username, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_login_fails_with_include_in_staff_group(self):
        # Setup
        self.user.groups.clear()
        self.user.groups.add(Group.objects.get(name="Staff"))
        data = {"username": self.user.username, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_login_fails_the_username_does_not_exist(self):
        # Setup
        error_message = "ERRORS_USERNAME_NOT_FOUND"
        data = {
            "username": self.another_user_attrs["username"],
            "password": self.valid_password,
        }
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data["error_code"], error_message)

    def test_login_fails_when_the_email_does_not_exist(self):
        # Setup
        error_message = "ERRORS_EMAIL_NOT_FOUND"
        data = {
            "email": self.another_user_attrs["email"],
            "password": self.valid_password,
        }
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data["error_code"], error_message)

    def test_login_fails_when_the_password_is_wrong(self):
        # Setup
        error_message = "ERRORS_WRONG_PASSWORD"
        data = {
            "email": self.user.email,
            "password": self.another_user_attrs["password"],
        }
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data["error_code"], error_message)

    def test_user_in_an_organization_login_successfully(self):
        # Setup
        admin_group = Group.objects.get(name="Admin")
        self.user.groups.add(admin_group)
        UserOrganizationGroupProxy.objects.create(
            user=self.user,
            organization=self.organization,
            group=admin_group,
        )

        data = {"email": self.user.email, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        response_organization = response.data.get("user").get("organization")
        response_group = response.data.get("user").get("group")
        # Verify

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("user").get("username"), self.user.username)
        self.assertEqual(
            response_organization.get("name"),
            self.organization.name,
        )
        self.assertEqual(
            response_organization.get("email"),
            self.organization.email,
        )
        self.assertEqual(
            response_organization.get("website"),
            self.organization.website,
        )
        self.assertEqual(
            response_organization.get("additional_info"),
            self.organization.additional_info,
        )
        self.assertEqual(
            response_organization.get("country").get("name"),
            self.organization.country.name,
        )
        self.assertEqual(
            response_organization.get("country").get("iso_code"),
            self.organization.country.code,
        )
        self.assertEqual(
            response_organization.get("language").get("id"),
            self.organization.language.id,
        )
        self.assertEqual(
            response_organization.get("language").get("name"),
            self.organization.language.name,
        )
        self.assertEqual(
            response_organization.get("language").get("iso_code"),
            self.organization.language.iso_code,
        )
        self.assertEqual(
            response_group.get("name"),
            admin_group.name,
        )
        self.assertEqual(
            response_group.get("id"),
            admin_group.id,
        )

    def test_user_without_an_organization_login_successfully(self):
        data = {"email": self.user.email, "password": self.valid_password}
        # Exercise
        response = self.client.post(self.url, data=data, format="json")
        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("user").get("username"), self.user.username)
        self.assertEqual(response.data.get("user").get("organization"), None)
