from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy
from apps.accounts.models import User, Language, UserInvitation, Region
from django.contrib.auth.models import Group
from apps.accounts.form import UserInvitationCreatedForm


@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ("id", "username", "email", "full_name", "is_staff")
    list_filter = ("created_at",)
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2"),
            },
        ),
        (
            gettext_lazy("users_admin_personal_info"),
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language"),
            },
        ),
        (
            gettext_lazy("users_admin_user_permissions"),
            {
                "classes": ("wide",),
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            gettext_lazy("users_prop_nc_app_password"),
            {"classes": ("wide",), "fields": ("nc_app_password",)},
        ),
    )
    fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "password",
                ),
            },
        ),
        (
            gettext_lazy("users_admin_personal_info"),
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language"),
            },
        ),
        (
            gettext_lazy("users_admin_user_permissions"),
            {
                "classes": ("wide",),
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            gettext_lazy("users_prop_nc_app_password"),
            {"classes": ("wide",), "fields": ("nc_app_password",)},
        ),
        (gettext_lazy("last_login"), {"classes": ("",), "fields": ("last_login",)}),
    )

    def delete_queryset(self, request, queryset):
        # Overwrite this method to ensure that the model delete is used.
        for obj in queryset:
            obj.delete()


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "iso_code",
    )


@admin.register(UserInvitation)
class UserInvitationAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    exclude = (
        "password",
        "is_superuser",
        "last_login",
        "is_active",
        "is_staff",
        "user_permissions",
    )
    list_display = ("username", "email")
    search_fields = (
        "username",
        "email",
    )
    fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "groups",
                    "username",
                ),
            },
        ),
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language", "region"),
            },
        ),
    )
    add_fieldsets = (
        (
            {
                "fields": (
                    "groups",
                    "username",
                ),
            },
        ),
        (
            {
                "classes": ("wide",),
                "fields": ("full_name", "email", "language"),
            },
        ),
    )

    def get_form(self, request, obj=None, **kwargs):
        # Show a OrganizationForm custom in creation
        if not obj:
            return UserInvitationCreatedForm
        return super(UserInvitationAdmin, self).get_form(request, obj, **kwargs)
