from django import forms
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import (
    PasswordResetForm as _PasswordResetForm,
    SetPasswordForm as _PasswordResetConfirmForm,
)
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.models import Site
from apps.accounts.models import User, UserInvitation, Language
from apps.sites.common import build_site_with_token
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from colmena.settings.base import (
    BACKEND_SITE_NAME,
    BACKEND_PASSWORD_RESET_SITE_NAME,
)
from django.utils.translation import gettext_lazy


class UserInvitationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserInvitationForm, self).__init__(*args, **kwargs)
        # TODO: Make UserInvitation supports groups as a one to many relationship
        self.fields["assigned_group"].initial = self.instance.groups.first().name
        self.fields["region"].initial = self.instance.region.name

    error_messages = {
        "password_mismatch": gettext_lazy("The two password fields didn’t match."),
    }
    new_password = forms.CharField(
        label=gettext_lazy("New password"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password_confirmation = forms.CharField(
        label=gettext_lazy("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
    )

    assigned_group = forms.CharField(
        label=gettext_lazy("Account Type"),
        disabled=True,
    )

    region = forms.CharField(
        label=gettext_lazy("Region"),
        disabled=True,
    )

    class Meta:
        model = UserInvitation
        fields = ["full_name", "username", "email", "language"]

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "full_name",
                    "username",
                    "email",
                    "language",
                    "new_password",
                    "new_password_confirmation",
                ),
            },
        ),
        (
            None,
            {
                "fields": ("assigned_group", "region"),
            },
        ),
    )

    def clean_new_password_confirmation(self):
        new_password = self.cleaned_data.get("new_password")
        new_password_confirmation = self.cleaned_data.get("new_password_confirmation")
        if (
            new_password
            and new_password_confirmation
            and new_password != new_password_confirmation
        ):
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        password_validation.validate_password(new_password_confirmation, self.instance)
        return new_password_confirmation

    def save(self, commit=True):
        user = super().save(commit=False)
        password = self.cleaned_data["new_password"]
        language = self.cleaned_data["language"].first()
        user.set_password(password)

        # FIXME: Add a function in UserInvitation model
        if not user.is_active:
            mailing_client = MailingClient()
            context = {
                "user": user,
                "support_web": "https://colmena.dw.com/support",
                "support_team": "Colmena Team",
                "confirmation_link": Site.objects.get(name=BACKEND_SITE_NAME).domain,
                "recovery_link": "https://colmena.dw.com/recovery",
            }

            email_template = mailing_client.get_template_by_language(
                "staff-confirmation", language.iso_code
            )

            email = mailing_client.build(
                get_noreply_from(),
                [user.email],
                email_template,
                context,
            )

            email.send()

        user.is_active = True
        user.is_staff = True
        user.language.clear()
        user.language.add(language.pk)
        user.save()
        return user


class UserInvitationCreatedForm(forms.ModelForm):
    groups = forms.ModelChoiceField(
        queryset=Group.objects.filter(name__in=["Superadmin", "Staff"]), required=True
    )
    language = forms.ModelChoiceField(queryset=Language.objects.all(), required=True)

    class Meta:
        model = UserInvitation
        fields = ["groups", "username", "full_name", "email", "language", "region"]

    def clean_groups(self):
        group = self.cleaned_data["groups"]
        return Group.objects.filter(id=group.id)

    def clean_language(self):
        language = self.cleaned_data["language"]
        return Language.objects.filter(id=language.id)


class PasswordResetForm(_PasswordResetForm):
    email = forms.EmailField(
        label=("Email"),
        max_length=254,
        widget=forms.EmailInput(attrs={"autocomplete": "email"}),
    )

    def save(self):
        # forces fields validation not triggered since we don't have a model associated to the form
        self.full_clean()
        email_address = self.cleaned_data.get("email")

        user = User.objects.get(email=email_address)
        language = user.language.first()

        if language is None:
            user_language = "en"
        else:
            user_language = language.iso_code

        token = PasswordResetTokenGenerator().make_token(user)
        url_path = Site.objects.get(name=BACKEND_PASSWORD_RESET_SITE_NAME).domain
        restore_password_link = build_site_with_token(
            url_path, {"pk": user.pk, "token": token}
        )
        self.send_mail(user, email_address, user_language, restore_password_link)

    def send_mail(
        self,
        user,
        email_address,
        user_language,
        restore_password_link,
    ):
        mail_client = MailingClient()
        email_template = mail_client.get_template_by_language(
            "user-password-reset", user_language
        )
        context = {
            "user": user,
            "support_team": "Colmena Team",
            "recovery_link": restore_password_link,
        }
        email = mail_client.build(
            get_noreply_from(),
            [email_address],
            email_template,
            context,
        )
        email.send()
