def is_superadmin_or_staff(user):
    return user.belongs_to_group("Superadmin") or user.belongs_to_group("Staff")
