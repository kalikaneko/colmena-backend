import os

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from apps.accounts.models import User, Language
from django.contrib.auth.models import Group

from colmena.settings.base import SUPERADMIN_EMAIL, SUPERADMIN_PASSWORD

from apps.nextcloud.config import get_nextcloud_api_wrapper_url


class Command(BaseCommand):
    help = "Creates a Superadmin with an app password for the Colmena instance"

    def add_arguments(self, parser):
        parser.add_argument(
            "superadmin_email", type=str, help="The Colmena superadmin email"
        )
        parser.add_argument(
            "superadmin_password", type=str, help="The Colmena superadmin password"
        )
        parser.add_argument(
            "nc_admin_username", type=str, help="The nextcloud admin username"
        )
        parser.add_argument(
            "nc_admin_password", type=str, help="The nextcloud admin password"
        )

    def handle(self, *args, **options):
        if os.environ.get("SUPERADMIN_PASSWORD_FORCE", "0") == "0":
            if User.objects.filter(email=SUPERADMIN_EMAIL).exists():
                self.stdout.write(
                    self.style.WARNING("Skipping Superadmin creation, already exists.")
                )
                return None

        nc_username = options["nc_admin_username"]
        nc_password = options["nc_admin_password"]
        superadmin_username = SUPERADMIN_EMAIL.split("@")[0]
        superadmin_full_name = "Colmena " + str.capitalize(superadmin_username)

        app_password = self.create_app_password(nc_username, nc_password)

        superadmin, created = User.objects.get_or_create(email=SUPERADMIN_EMAIL)

        if created:
            superadmin.username = superadmin_username
            superadmin.full_name = superadmin_full_name
            superadmin.nc_app_password = app_password
            superadmin.is_active = True
            superadmin.is_staff = True
            superadmin.is_superuser = True
            superadmin.groups.add(Group.objects.get(name="Superadmin"))
            superadmin.language.add(Language.objects.get(name="English"))
            superadmin.set_password(SUPERADMIN_PASSWORD)

        else:
            superadmin.nc_app_password = app_password
            superadmin.is_active = True

        superadmin.save()
        self.stdout.write(self.style.SUCCESS("Successfully created admin user"))
        return None

        return None

    def create_app_password(self, username, password):
        return call_command("create_app_password", username, password)
