from apps.accounts.models import UserInvitation, User, Language
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from colmena.settings.base import BACKEND_SITE_NAME

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from django.urls import reverse


@receiver(m2m_changed, sender=User.language.through)
def send_email_invitation(sender, instance, pk_set, action, model, **kwargs):
    if (
        # FIXME: To limit future calls after language changes
        not instance.is_active
        # FIXME: To limit in the creation of UserInvitation
        and isinstance(instance, UserInvitation)
        and action == "post_add"
        and pk_set
    ):
        languague_pk = pk_set.pop()
        domain = Site.objects.get(name=BACKEND_SITE_NAME).domain
        language = Language.objects.get(pk=languague_pk)
        mailing_client = MailingClient()
        token = PasswordResetTokenGenerator().make_token(instance)
        invitation_link = (
            f'{domain}{reverse("invitations_register",args=[instance.pk,token])}'
        )
        # TODO: The template should use the user's languages
        email_template = mailing_client.get_template_by_language(
            "staff-invitation", language.iso_code
        )
        context = {
            "user": instance,
            "role": "Admin",
            "region": instance.region.name,
            "invitation_link": invitation_link,
        }
        email = mailing_client.build(
            get_noreply_from(),
            [instance.email],
            email_template,
            context,
        )

        email.send()
