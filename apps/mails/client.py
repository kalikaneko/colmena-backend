from django.core.mail import EmailMessage, send_mail
from templated_email import get_templated_mail
from django.contrib.sites.models import Site

from colmena.settings.base import BACKEND_SITE_NAME


class MailingClient:
    TEMPLATE_SUFFIX = "email.html"
    email = None

    def build(self, from_email, to, template, context, **options):
        context["backend_host"] = Site.objects.get(name=BACKEND_SITE_NAME).domain
        self.email = get_templated_mail(
            template_name=template,
            from_email=from_email,
            to=to,
            context=context,
            template_suffix=self.TEMPLATE_SUFFIX,
            # Optional:
            # cc=['cc@example.com'],
            # bcc=['bcc@example.com'],
            # headers={'My-Custom-Header':'Custom Value'},
            # template_prefix="my_emails/",
        )

        return self.email

    def send(self):
        return self.email.send()

    def get_template_by_language(self, templated_email, iso_code):
        if iso_code in self.available_templates():
            return f"{iso_code}/{templated_email}"
        else:
            return f"en/{templated_email}"

    def available_templates(self):
        return ["en", "es", "pt"]
