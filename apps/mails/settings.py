import os


def get_noreply_from():
    return os.environ.get("EMAIL_FROM", "no-reply@some.email")
