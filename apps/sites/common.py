from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode as uid_encoder


def build_site_with_token(url_path, args, **kwargs):
    uidb64 = uid_encoder(force_bytes(args["pk"]))
    token = args["token"]
    site_url = (
        url_path.replace("<pk>", uidb64)
        .replace("<uid>", uidb64)
        .replace("<token>", token)
    )
    return site_url
