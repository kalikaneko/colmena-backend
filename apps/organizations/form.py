from django import forms
from django.contrib.auth import password_validation
from .models import Organization, UserOrganizationGroupProxy
from apps.accounts.models import User, Language, UserInvitation
from django.utils.translation import gettext_lazy
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group
from django_countries.widgets import CountrySelectWidget


class OrganizationForm(forms.ModelForm):
    error_messages = {
        "email_exists": gettext_lazy("We have a user with this email"),
        "username_exists": gettext_lazy("We have a user with this username"),
    }

    admin_email = forms.EmailField(label=gettext_lazy("admin_user_email"))

    admin_full_name = forms.CharField(
        label=gettext_lazy("admin_full_name"),
        max_length=80,
    )

    admin_username = forms.CharField(
        label=gettext_lazy("admin_username"),
        max_length=30,
    )

    admin_language = forms.ModelChoiceField(
        queryset=Language.objects.all(), label=gettext_lazy("admin_language")
    )

    class Meta:
        model = Organization
        fields = [
            "name",
            "country",
        ]
        widgets = {"country": CountrySelectWidget()}

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "admin_email",
                    "admin_full_name",
                    "admin_language",
                    "country",
                ),
            },
        ),
    )

    def clean_admin_email(self):
        new_email = self.cleaned_data.get("admin_email")
        # Check don't exist user with this email
        if User.objects.filter(email=new_email).exists():
            raise forms.ValidationError(
                self.error_messages["email_exists"],
            )
        return new_email

    def clean_admin_username(self):
        new_username = self.cleaned_data.get("admin_username")
        # Check don't exist user with username
        if User.objects.filter(username__iexact=new_username).exists():
            raise forms.ValidationError(
                self.error_messages["username_exists"],
            )
        return new_username

    def save(self, commit=True):
        # Handle Errors
        if self.errors:
            raise ValueError(
                "The %s could not be %s because the data didn't validate."
                % (
                    self.instance._meta.object_name,
                    "created" if self.instance._state.adding else "changed",
                )
            )
        cleaned_data = self.cleaned_data
        group_admin = Group.objects.get(name="OrgOwner")
        # Create new Organization
        self.instance.language = cleaned_data["admin_language"]
        self.instance.save()

        # Create new User
        new_user = User.objects.create(
            username=cleaned_data["admin_username"],
            email=cleaned_data["admin_email"],
            full_name=cleaned_data["admin_full_name"],
        )
        new_user.save()
        new_user.language.add(cleaned_data["admin_language"])
        new_user.groups.add(group_admin)
        # Create UserOrganizationProxy
        UserOrganizationGroupProxy.objects.create(
            user=new_user,
            organization=self.instance,
            group=group_admin,
        )

        return self.instance

    def save_m2m(self):
        return self._save_m2m()
