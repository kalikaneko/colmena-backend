from apps.accounts.models import User
from apps.organizations.models import (
    Organization,
    UserOrganizationGroupProxy,
    Team,
    UserTeam,
)
from django.contrib import admin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy
from .form import OrganizationForm


# Register your models here.
@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    # Add any customizations for the Organization admin view, if needed
    list_display = ("name", "created_by")  # Show the 'name' field in the list view

    def get_form(self, request, obj=None, **kwargs):
        # Show a OrganizationForm custom in creation
        if not obj:
            return OrganizationForm
        return super(OrganizationAdmin, self).get_form(request, obj, **kwargs)

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)

        # Show for only Superadmin Users
        if (
            request.user.groups.filter(name="Superadmin").exists()
            or request.user.is_superuser
        ):
            return fields

        # Limit a Staff Users not view a created_by field
        fields = [field for field in fields if field != "created_by"]
        return fields

    def save_model(self, request, obj, form, change):
        # Save user creator Organization
        obj.created_by = request.user
        super().save_model(request, obj, form, change)

    def get_fields(self, request, obj=None):
        if obj:
            return (
                "name",
                "country",
                "language",
                "email",
                "website",
                "avatar",
                "additional_info",
                "created_by",
            )
        else:
            return super().get_fields(request, obj)

    def get_queryset(self, request):
        user = request.user
        queryset = Organization.objects.none()
        if request.user.belongs_to_group("Staff"):
            queryset = super().get_queryset(request).filter(created_by=request.user)
        # TODO: Implement a Region filter for users that belong to the Superadmin group
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = super().get_queryset(request)

        return queryset


@admin.register(UserOrganizationGroupProxy)
class UserOrganizationGroupProxyAdmin(admin.ModelAdmin):
    # Add customizations for the UserOrganizationGroupProxy admin view
    list_display = ("user", "organization", "group")
    list_filter = ("organization",)
    search_fields = (
        "user__username",
        "user__email",
    )  # You can add other fields for searching users
    add_fieldsets = (
        gettext_lazy("Organization/User management"),
        {"classes": ("wide",), "fields": ("user", "organization", "group")},
    )

    def save_model(self, request, obj, form, change):
        # Custom logic to handle changes in the group selection and save the model
        # Here, you can implement any additional logic you need when changing the group for users
        super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        user = request.user
        queryset = Organization.objects.none()
        if request.user.belongs_to_group("Staff"):
            queryset = (
                super()
                .get_queryset(request)
                .filter(organization__created_by=request.user)
            )
        # TODO: Implement a Region filter for users that belong to the Superadmin group
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = super().get_queryset(request)
        return queryset


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        user = request.user
        queryset = Team.objects.none()
        if request.user.belongs_to_group("Staff"):
            queryset = super().get_queryset(request).filter(created_by=request.user)
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = super().get_queryset(request)

        return queryset


@admin.register(UserTeam)
class UserTeamAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        user = request.user
        queryset = UserTeam.objects.none()
        if request.user.belongs_to_group("Staff"):
            queryset = super().get_queryset(request).filter(created_by=request.user)
        elif user.is_superuser or user.belongs_to_group("Superadmin"):
            queryset = super().get_queryset(request)

        return queryset
