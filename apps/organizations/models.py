from django.contrib import admin
from django.contrib.auth.models import Group
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy
from django.contrib.auth import get_user_model
from colmena.settings import base
from apps.accounts.models import User, Language
from django_countries.fields import CountryField
from colmena.validators.validators import ValidatorOnlyLettersAndSpaces


class Organization(models.Model):
    # Add your organization fields here
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=gettext_lazy("organization_prop_name"),
        validators=[ValidatorOnlyLettersAndSpaces()],
    )

    created_by = models.ForeignKey(
        User,
        on_delete=models.RESTRICT,
        verbose_name=gettext_lazy("organization_fk_user_name"),
        null=True,
    )

    email = models.EmailField(
        blank=True,
        verbose_name=gettext_lazy("organization_prop_email_address"),
        max_length=80,
    )

    language = models.ForeignKey(
        Language,
        on_delete=models.RESTRICT,
        verbose_name=gettext_lazy("organization_fk_languages_name"),
        null=True,
    )

    website = models.URLField(
        blank=True, verbose_name=gettext_lazy("organization_prop_website")
    )

    country = CountryField(
        blank=False,
        default="ES",
        verbose_name=gettext_lazy("organization_prop_country"),
    )

    avatar = models.ImageField(upload_to="avatars/", null=True, blank=True)

    additional_info = models.TextField(
        blank=True, verbose_name=gettext_lazy("organization_prop_additional_info")
    )

    def __str__(self):
        return self.name

    @classmethod
    def is_valid_pk(cls, pk):
        try:
            return cls.objects.filter(pk=pk).exists()
        except Exception as e:
            return False

    def get_organization_owner_user(self):
        try:
            return UserOrganizationGroupProxy.objects.get(
                organization=self, group__name="OrgOwner"
            ).user
        except UserOrganizationGroupProxy.DoesNotExist:
            return None

    class Meta:
        verbose_name = gettext_lazy("organization_model_name")
        verbose_name_plural = gettext_lazy("organization_model_plural_name")


class UserOrganizationGroup(models.Model):
    user = models.OneToOneField(
        base.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("user_organization_group_fk_user_name"),
    )
    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("user_organization_group_fk_organization_name"),
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("user_organization_group_fk_group_name"),
    )
    created_at = models.DateTimeField(
        gettext_lazy("created_at"), default=timezone.now, editable=False
    )
    updated_at = models.DateTimeField(
        gettext_lazy("updated_at"), default=timezone.now, editable=True
    )

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        super(UserOrganizationGroup, self).save(*args, **kwargs)

    class Meta:
        unique_together = ("organization", "group", "user")
        verbose_name = gettext_lazy("user_organization_group_model_name")
        verbose_name_plural = gettext_lazy("user_organization_group_model_plural_name")


# Create a proxy model for UserOrganizationGroup
class UserOrganizationGroupProxy(UserOrganizationGroup):
    class Meta:
        proxy = True
        verbose_name = gettext_lazy("Organization/User management")
        verbose_name_plural = gettext_lazy("Organizations/Users management")


class SocialMedia(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name=gettext_lazy("social_media_prop_name"),
    )

    url = models.URLField(
        max_length=255, unique=True, verbose_name=gettext_lazy("social_media_prop_url")
    )

    owner_organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("social_media_fk_organization_name"),
    )


class Team(models.Model):
    nc_group_id = models.CharField(
        max_length=255,
        verbose_name=gettext_lazy("team_group_id_prop"),
    )

    nc_conversation_token = models.CharField(
        max_length=255,
        verbose_name=gettext_lazy("team_conversation_token_prop"),
    )

    is_personal_workspace = models.BooleanField(
        default=False,
        verbose_name=gettext_lazy("team_is_personal_workspace_prop"),
    )

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("team_conversation_fk_organization_name"),
        null=True,
    )

    def __str__(self):
        return self.nc_group_id


class UserTeam(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("user_team_fk_id_user_name"),
    )

    team = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        verbose_name=gettext_lazy("user_team_fk_id_team"),
    )

    def __str__(self):
        return f"{self.user.full_name}-{self.team.nc_group_id}"
