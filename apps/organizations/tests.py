import io

from django.test import TestCase, tag
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Group
from django.urls import reverse
from django.utils.translation import gettext_lazy
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files.storage import default_storage

from rest_framework.test import APITestCase
from rest_framework import status


from PIL import Image

from apps.accounts.models import Language, User
from apps.organizations.form import OrganizationForm
from apps.organizations.support import organization_fixture
from apps.accounts.support import user_fixture, language_fixture
from apps.organizations.models import Organization, UserOrganizationGroupProxy
from django_countries import countries


class TestOrganizationModel(TestCase):
    fixtures = [
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
    ]

    def setUp(self):
        # FIXME: Create a dummy avatar
        buffer = io.BytesIO()
        image = Image.new("RGB", (100, 100), color="red")
        image.save(buffer, format="JPEG")
        self.image_file = SimpleUploadedFile("test.jpg", buffer.getvalue())
        self.user = user_fixture.create()

    def tearDown(self):
        self.image_file.close()

    def test_create_organization_with_valid_attrs_returns_a_new_organization(self):
        valid_attrs = organization_fixture.valid_attrs()
        organization = organization_fixture.create(attrs=valid_attrs)
        organization.avatar = self.image_file
        organization.save()

        self.assertEqual(organization.email, valid_attrs["email"])
        self.assertEqual(organization.name, valid_attrs["name"])
        self.assertEqual(organization.website, valid_attrs["website"])
        self.assertEqual(organization.country, valid_attrs["country"])
        self.assertEqual(organization.additional_info, valid_attrs["additional_info"])
        self.assertEqual(organization.language, valid_attrs["language"])

        self.assertIsNotNone(organization.avatar)
        file_path = organization.avatar.path
        self.assertTrue(default_storage.exists(file_path))


class TestOrganizationForm(TestCase):
    fixtures = [
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/accounts/seeds/04-languages.json",
        "apps/sites/seeds/00-sites.json",
    ]

    def setUp(self):
        self.language = Language.objects.get(iso_code="es")

    def test_is_submitted_successfully(self):
        # Setup
        org_attrs = organization_fixture.valid_attrs()
        user_attrs = user_fixture.valid_attrs()

        form_data = {
            "name": org_attrs["name"],
            "admin_username": user_attrs["username"],
            "admin_email": user_attrs["email"],
            "admin_full_name": user_attrs["full_name"],
            "admin_language": self.language,
            "country": org_attrs["country"],
        }
        form = OrganizationForm(data=form_data)

        # Verify
        self.assertTrue(form.is_valid())

        # Exercise
        form.save()
        form = form.save_m2m()

        # Verify
        new_org = Organization.objects.get(name=form_data["name"])
        self.assertIsInstance(new_org, Organization)
        self.assertEqual(new_org.name, form_data["name"])

        new_usr = User.objects.get(email=form_data["admin_email"])
        self.assertIsInstance(new_usr, User)
        self.assertEqual(new_usr.username, form_data["admin_username"])
        self.assertEqual(new_usr.full_name, form_data["admin_full_name"])

        new_rel = UserOrganizationGroupProxy.objects.get(user=new_usr)
        self.assertIsInstance(new_rel, UserOrganizationGroupProxy)
        self.assertEquals(new_rel.organization, new_org)
        self.assertEquals(new_rel.user, new_usr)
        self.assertEquals(new_rel.group.name, "OrgOwner")

    def test_fails_when_the_organization_name_exists(self):
        error_msg = "Organization with this Organization Name already exists."
        org = organization_fixture.create()
        user_attrs = user_fixture.valid_attrs()
        form_data = {
            "name": org.name,
            "admin_username": user_attrs["username"],
            "admin_email": user_attrs["email"],
            "admin_full_name": user_attrs["full_name"],
            "admin_language": self.language,
            "country": org.country,
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["name"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertIsInstance(org, Organization)
        self.assertEqual(org.name, form_data["name"])
        self.assertEqual(Organization.objects.count(), 1)

        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_email"])

        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization=org)

    def test_fails_when_the_organization_name_contains_digits_or_special_character(
        self,
    ):
        error_msg = "The field must contains letters and spaces"
        name = "col1mena_"
        org = organization_fixture.create({"name": name})
        user_attrs = user_fixture.valid_attrs()
        form_data = {
            "name": org.name,
            "admin_username": user_attrs["username"],
            "admin_email": user_attrs["email"],
            "admin_full_name": user_attrs["full_name"],
            "admin_language": self.language,
            "country": org.country,
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["name"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertIsInstance(org, Organization)
        self.assertEqual(org.name, form_data["name"])
        self.assertEqual(Organization.objects.count(), 1)

        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_email"])

        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization=org)

    def test_fails_when_the_email_exists(self):
        error_msg = "We have a user with this email"
        org_attrs = organization_fixture.valid_attrs()
        new_usr = user_fixture.create()
        form_data = {
            "name": org_attrs["name"],
            "admin_username": "another username",
            "admin_email": new_usr.email,
            "admin_full_name": "admin_full_name",
            "admin_language": self.language,
            "country": org_attrs["country"],
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors["admin_email"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertEquals(Organization.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            Organization.objects.get(name=form_data["name"])

        self.assertEquals(User.objects.count(), 1)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        self.assertEquals(UserOrganizationGroupProxy.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization__name=form_data["name"])

    def test_fails_when_the_username_exists(self):
        error_msg = "We have a user with this username"
        org_attrs = organization_fixture.valid_attrs()
        new_usr = user_fixture.create()
        form_data = {
            "name": org_attrs["name"],
            "admin_username": new_usr.username,
            "admin_email": "anotheremail@email.net",
            "admin_full_name": "admin_full_name",
            "admin_language": self.language,
            "country": org_attrs["country"],
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        with self.assertRaises(ValueError):
            form.save()

        self.assertEquals(form.errors["admin_username"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertEquals(Organization.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            Organization.objects.get(name=form_data["name"])

        self.assertEquals(User.objects.count(), 1)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        self.assertEquals(UserOrganizationGroupProxy.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization__name=form_data["name"])

    def test_fails_when_the_username_already_exists(self):
        error_msg = "We have a user with this username"
        org_attrs = organization_fixture.valid_attrs()
        new_usr = user_fixture.create()
        form_data = {
            "name": org_attrs["name"],
            "admin_username": new_usr.username.upper(),
            "admin_email": "anotheremail@email.net",
            "admin_full_name": "admin_full_name",
            "admin_language": self.language,
            "country": org_attrs["country"],
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        with self.assertRaises(ValueError):
            form.save()

        self.assertEquals(form.errors["admin_username"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertEquals(Organization.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            Organization.objects.get(name=form_data["name"])

        self.assertEquals(User.objects.count(), 1)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        self.assertEquals(UserOrganizationGroupProxy.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization__name=form_data["name"])

    def test_fails_when_the_language_is_not_present(self):
        error_msg = "This field is required."
        org_attrs = organization_fixture.valid_attrs()
        user_attrs = user_fixture.valid_attrs()
        form_data = {
            "name": org_attrs["name"],
            "admin_username": user_attrs["username"],
            "admin_email": user_attrs["email"],
            "admin_full_name": user_attrs["full_name"],
            "country": org_attrs["country"],
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        with self.assertRaises(ValueError):
            form.save()

        self.assertEquals(form.errors["admin_language"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertEquals(Organization.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            Organization.objects.get(name=form_data["name"])

        self.assertEquals(User.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        self.assertEquals(UserOrganizationGroupProxy.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization__name=form_data["name"])

    def test_fails_when_the_country_is_not_present(self):
        error_msg = "This field is required."
        org_attrs = organization_fixture.valid_attrs()
        user_attrs = user_fixture.valid_attrs()
        form_data = {
            "name": org_attrs["name"],
            "admin_username": user_attrs["username"],
            "admin_email": user_attrs["email"],
            "admin_full_name": user_attrs["full_name"],
            "admin_language": self.language,
        }
        form = OrganizationForm(data=form_data)

        self.assertFalse(form.is_valid())
        with self.assertRaises(ValueError):
            form.save()
        self.assertEquals(form.errors["country"], [error_msg])
        # check that the list of errors has only one element
        self.assertEquals(form.errors.__len__(), 1)

        self.assertEquals(Organization.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            Organization.objects.get(name=form_data["name"])

        self.assertEquals(User.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email=form_data["admin_username"])

        self.assertEquals(UserOrganizationGroupProxy.objects.count(), 0)
        with self.assertRaises(ObjectDoesNotExist):
            UserOrganizationGroupProxy.objects.get(organization__name=form_data["name"])


class TestOrganizationViewSet(APITestCase):
    fixtures = [
        "apps/accounts/seeds/01-permissions.json",
        "apps/accounts/seeds/02-groups.json",
        "apps/sites/seeds/00-sites.json",
    ]

    def setUp(self):
        self.user = user_fixture.create()
        self.language = language_fixture.create()
        self.user.language.add(self.language)
        self.organization = organization_fixture.create({"language": self.language})
        self.url = reverse("organization-detail", args=[self.organization.pk])
        self.user_group = Group.objects.get(name="User")
        self.admin_organization_relationship = (
            UserOrganizationGroupProxy.objects.create(
                user=self.user,
                organization=self.organization,
                group=Group.objects.get(name="Admin"),
            )
        )

        self.valid_attrs_request = organization_fixture.valid_attrs(
            {"language": self.language.pk}
        )

    def test_enabled_get_and_failed_anothers_requests_methods(self):
        # Setup endpoint and data dummy

        data = {"name": self.organization.name, "created_by": self.user.pk}

        # Login
        self.client.force_authenticate(user=self.user)

        response = self.client.put(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.delete(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.post(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Exercise
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("name"), self.organization.name)

    def test_get_organization_with_no_member_user(self):
        # Error message
        error_user_not_member = "ERRORS_USER_IS_NOT_ORGANIZATION_MEMBER"
        # Set same language to user
        self.organization.language = self.language
        self.organization.save()

        # Create another user
        another_user = user_fixture.create()

        # Login
        self.client.force_authenticate(user=another_user)

        # Exercise
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data["error_code"], error_user_not_member)

    def test_organization_admin_user_retrieves_its_own_organization_details(self):
        url = reverse("organization-byuser")

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(url)

        # Verify
        self.assertEqual(response.data.get("name"), self.organization.name)
        self.assertEqual(response.data.get("email"), self.organization.email)
        self.assertEqual(response.data.get("website"), self.organization.website)
        self.assertEqual(
            response.data.get("country").get("name"), self.organization.country.name
        )
        self.assertEqual(
            response.data.get("country").get("iso_code"), self.organization.country.code
        )

    def test_organization_user_retrieves_its_own_organization_details(self):
        # Setup
        user = user_fixture.create()
        user.language.add(self.language)
        UserOrganizationGroupProxy.objects.create(
            user=user,
            organization=self.organization,
            group=Group.objects.get(name="User"),
        )
        url = reverse("organization-byuser")

        # Login
        self.client.force_authenticate(user=user)

        # Exercise
        response = self.client.get(url)

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("name"), self.organization.name)
        self.assertEqual(response.data.get("email"), self.organization.email)
        self.assertEqual(response.data.get("website"), self.organization.website)
        self.assertEqual(
            response.data.get("country").get("name"), self.organization.country.name
        )
        self.assertEqual(
            response.data.get("country").get("iso_code"), self.organization.country.code
        )

    def test_fails_when_the_current_user_is_not_assigned_to_an_organization(self):
        # Create another user
        error_msg = "ERRORS_NO_ORGANIZATION_FOUND_FOR_THE_CURRENT_USER"
        user = user_fixture.create()
        url = reverse("organization-byuser")

        # Login
        self.client.force_authenticate(user=user)

        # Exercise
        response = self.client.get(url)

        # Verify
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data["error_code"], error_msg)

    def test_successfully_updates_a_organization_when_attrs_are_valid(self):
        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(
            self.url, data=self.valid_attrs_request, format="json"
        )
        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_a_non_admin_organization_fails_update_organization(self):
        # Setup
        member_organization_user = user_fixture.create()
        member_organization_user.language.add(self.language)
        UserOrganizationGroupProxy.objects.create(
            user=member_organization_user,
            organization=self.organization,
            group=self.user_group,
        )

        # Login
        self.client.force_authenticate(user=member_organization_user)

        # Exercise
        response = self.client.patch(
            self.url, data=self.valid_attrs_request, format="json"
        )

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_fails_with_bad_request_when_the_country_is_not_valid(self):
        # Setup
        error_invalid_country = "ERRORS_INVALID_COUNTRY"
        invalid_request = organization_fixture.valid_attrs(
            {"country": "invalid_country"}
        )
        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=invalid_request, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["country"][0], error_invalid_country)

    def test_fails_when_the_website_format_is_not_valid(self):
        # Setup
        error_invalid_website = "ERRORS_INVALID_WEBSITE_FORMAT"
        invalid_request = organization_fixture.valid_attrs(
            {"website": "invalid website"}
        )
        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.patch(self.url, data=invalid_request, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["website"][0], error_invalid_website)


class TestCountryViewSet(APITestCase):
    def setUp(self):
        self.user = user_fixture.create()
        self.countries = countries.countries

    def test_successfully_retrieves_a_country_list(self):
        # Setup
        url = reverse("countries")

        # Login
        self.client.force_authenticate(user=self.user)

        # Exercise
        response = self.client.get(url, format="json")
        data = response.data

        # Verify
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(data), len(self.countries.values()))
        self.assertEqual(data[0].get("name"), self.countries[data[0].get("iso_code")])
        self.assertEqual(
            data[248].get("name"), self.countries[data[248].get("iso_code")]
        )

    def test_a_non_authenticated_request_cannot_retrieve_a_country_list(self):
        # Setup
        url = reverse("countries")

        # Exercise
        response = self.client.get(url, format="json")

        # Verify
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
