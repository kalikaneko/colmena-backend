from django.apps import apps
from django.core.exceptions import ValidationError


def user_exists_in_organization(user_id):
    UserOrganizationGroupProxy = apps.get_model(
        "organizations", "UserOrganizationGroupProxy"
    )
    return UserOrganizationGroupProxy.objects.filter(user__id=user_id).exists()


def user_belongs_to_organization_group(user_id, organization_id, group_name):
    UserOrganizationGroupProxy = apps.get_model(
        "organizations", "UserOrganizationGroupProxy"
    )
    return UserOrganizationGroupProxy.objects.filter(
        user__id=user_id, organization__id=organization_id, group__name=group_name
    ).exists()


def get_personal_workspace(nextcloud_user_id):
    Team = apps.get_model("organizations", "Team")
    User = apps.get_model("accounts", "User")

    user = User.objects.get(username=nextcloud_user_id)

    try:
        return Team.objects.get(userteam__user_id=user.id, is_personal_workspace=True)
    except Exception as e:
        raise ValidationError("USER_GET_PERSONAL_WORKSPACE_TEAM_FAILED")
