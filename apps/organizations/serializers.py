from rest_framework import serializers
from django.contrib.auth.models import Group
from apps.organizations.models import Organization, UserOrganizationGroup, Team
from apps.accounts.models import Language, User
from drf_spectacular.utils import extend_schema_field


class CountrySerializer(serializers.Serializer):
    name = serializers.CharField()
    iso_code = serializers.CharField(source="code", required=False)


class OrganizationLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"


class UserOrganizationGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOrganizationGroup
        fields = (
            "group",
            "user",
            "created_at",
            "updated_at",
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["user"] = OrganizationMemberSerializer(instance.user).data
        representation["group"] = OrganizationMemberGroupSerializer(instance.group).data
        return representation


class OrganizationSerializer(serializers.ModelSerializer):
    members = serializers.SerializerMethodField()

    class Meta:
        model = Organization
        fields = (
            "id",
            "name",
            "email",
            "website",
            "country",
            "avatar",
            "additional_info",
            "created_by",
            "language",
            "members",
        )
        extra_kwargs = {
            "website": {"error_messages": {"invalid": "ERRORS_INVALID_WEBSITE_FORMAT"}},
            "country": {"error_messages": {"invalid_choice": "ERRORS_INVALID_COUNTRY"}},
            "language": {
                "error_messages": {"does_not_exist": "ERRORS_INVALID_LANGUAGE"}
            },
        }

    @extend_schema_field(field=UserOrganizationGroupSerializer)
    def get_members(self, instance):
        organization_members = UserOrganizationGroup.objects.filter(
            organization_id=instance.pk
        )
        return UserOrganizationGroupSerializer(organization_members, many=True).data

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["country"] = CountrySerializer(instance.country).data
        representation["language"] = OrganizationLanguageSerializer(
            instance.language
        ).data
        return representation


class OrganizationMemberGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            "id",
            "name",
        )


class OrganizationMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "pk",
            "username",
            "full_name",
            "email",
        )


class TeamSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    organization_id = serializers.PrimaryKeyRelatedField(
        read_only=True, allow_null=True
    )
    organization_name = serializers.CharField(
        source="organization.name", allow_null=True
    )

    class Meta:
        model = Team
        fields = (
            "id",
            "nc_group_id",
            "nc_conversation_token",
            "organization_id",
            "organization_name",
            "is_personal_workspace",
        )


class TeamRequestSerializer(serializers.Serializer):
    last_message = serializers.BooleanField(required=False, default=False)


class TeamListRequestSerializer(TeamRequestSerializer):
    skip_personal_workspace = serializers.BooleanField(required=False, default=False)


class TeamResponseSerializer(TeamSerializer):
    organization_id = serializers.IntegerField(allow_null=True)
    last_message = serializers.DictField(required=False)
    organization_name = serializers.CharField()

    class Meta(TeamSerializer.Meta):
        fields = TeamSerializer.Meta.fields + ("last_message",)


class TeamListSharedFilesResponseSerializer(serializers.Serializer):
    files = serializers.ListField(child=serializers.DictField())

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["quantity"] = len(representation["files"])
        return representation
