from .models import UserOrganizationGroupProxy
from apps.mails.client import MailingClient
from apps.mails.settings import get_noreply_from
from colmena.settings.base import FRONTEND_SIGNUP_SITE_NAME

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver
from urllib.parse import urlencode
from django.contrib.auth.models import Group


@receiver(post_save, sender=UserOrganizationGroupProxy)
def send_organization_invitation(sender, instance, created, **kwargs):
    base_template = "invitation-admin-organization"
    domain = Site.objects.get(name=FRONTEND_SIGNUP_SITE_NAME).domain
    mailing_client = MailingClient()
    new_user = instance.user
    token = PasswordResetTokenGenerator().make_token(new_user)
    query_params = urlencode({"username": new_user.username})
    # HACK: used to decouple the url needed for the frontend from the endpoint of the backend
    url = domain.replace("<pk>", str(new_user.pk)).replace("<token>", token)
    invitation_link = f"{url}?{query_params}"
    context = {
        "user": new_user,
        "organization": instance.organization,
        "support_web": "https://colmena.dw.com/support",
        "support_team": "Colmena Team",
        "invitation_link": invitation_link,
    }
    # HACK: Select a iso code for first language
    iso_code_email = new_user.language.first().iso_code
    # Check if exist a admin user in Organization
    if UserOrganizationGroupProxy.objects.filter(
        organization=instance.organization, group=Group.objects.get(name="OrgOwner")
    ).exists():
        base_template = "member-organization-invitation"

    email_template = mailing_client.get_template_by_language(
        base_template, instance.organization.language.iso_code
    )

    email = mailing_client.build(
        get_noreply_from(),
        [new_user.email],
        email_template,
        context,
    )
    email.send()
