from apps.nextcloud.resources import groups as nextcloud_group_manager
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import utils as nextcloud_utils

from django.core.exceptions import ValidationError


def initialise_conversation(group_name, nextcloud_user_id, user, auth_user):
    try:
        # Add user in group
        nextcloud_group_manager.add_user_to_group(
            group_id=group_name,
            user_id=nextcloud_user_id,
            user=auth_user,
        )
        # Provides conversation creation privileges to the Admin
        nextcloud_group_manager.promote_user_to_subadmin(
            group_id=group_name,
            user_id=nextcloud_user_id,
            user=auth_user,
        )
        # Create a conversation group
        nc_conversation = nextcloud_talk_manager.create_group_conversation(
            room_name=group_name, group_id=group_name, user=user
        )

        return nextcloud_utils.get_conversation_token(nc_conversation)
    except Exception as e:
        raise ValidationError("ERRORS_INITIALISE_CONVERSATION_FAILED")
