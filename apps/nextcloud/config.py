import os
import subprocess
from colmena.settings.base import (
    TALK_DIR,
    NEXTCLOUD_API_URL,
    NEXTCLOUD_API_WRAPPER_URL,
    NEXTCLOUD_ADMIN_GROUP_NAME,
)


def get_talk_dir():
    return TALK_DIR


def get_nextcloud_api_url():
    return NEXTCLOUD_API_URL


def get_nextcloud_api_wrapper_url():
    return NEXTCLOUD_API_WRAPPER_URL


def get_nextcloud_admin_group_name():
    return NEXTCLOUD_ADMIN_GROUP_NAME
