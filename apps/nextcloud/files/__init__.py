import os
from colmena.serializers.serializers import ErrorSerializer
from django.core.exceptions import ValidationError
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.resources import shares as nextcloud_shares_manager
from apps.nextcloud.resources import files as nextcloud_files_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud.config import get_talk_dir


TALK_DIR = get_talk_dir()


def send_as_attachment(new_file, filename, conversation_token, user):
    """
    Given a file, a filename, a conversation token and a user uploads and shares
    a file in the conversation associated to the given conversation token.

    Parameters
    ----------

    new_file : InMemoryUploadedFile
        A file in memory.

    filename : str
        A file name with extension.

    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        nextcloud_upload_path : str
    }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    try:
        upload_file = upload(new_file, filename, user)
        data = share(upload_file["nextcloud_upload_path"], conversation_token, user)
        return data
    except Exception as e:
        raise ValidationError(ErrorSerializer("ERRORS_SEND_ATTACHMENT").data)


def upload(file, filename, user):
    """
    Given a file and a user, uploads the file to the nextcloud instance.

    Parameters
    ----------

    file : InMemoryUploadedFile
        A file in memory.

    filename : str
        A file name with extension.

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        nextcloud_upload_path : String
    }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    try:
        local_path = nextcloud_utils.handle_uploaded_file(file)
        remote_path = os.path.join(TALK_DIR, filename)
        nextcloud_files_manager.upload(local_path, filename, user)
        return {"nextcloud_upload_path": remote_path}
    except Exception as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_UPLOAD_FILE").data)
    finally:
        if os.path.exists(local_path):
            os.remove(local_path)
        file.close()


def share(file_path, conversation_token, user):
    """
    Given a file_path, a conversation_token and a user to authenticate the request,
    sends a shared file in the conversation associated to the given conversation token.


    Parameters
    ----------

    file_path : str
        A valid path .

    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        share_id : Number,
        file_owner: str,
    }

    """
    try:
        data = nextcloud_shares_manager.create_conversation_share(
            file_path, conversation_token, user
        )
        return {"share_id": data["id"], "file_owner": data["displayname_file_owner"]}
    except Exception as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_SHARE_ERROR").data)


def get_available_extension():
    return [
        ".avi",
        ".bmp",
        ".csv",
        ".doc",
        ".docx",
        ".dwg",
        ".avi",
        ".flv",
        ".jpg",
        ".gif",
        ".jpeg",
        ".mdb",
        ".mid",
        ".mov",
        ".mp3",
        ".mp4",
        ".mpeg",
        ".pdf",
        ".png",
        ".ppt",
        ".rar",
        ".svg",
        ".tiff",
        ".wav",
        ".wma",
        ".xsl",
        ".zip",
    ]


def is_valid_extension(extension):
    return extension in get_available_extension()
