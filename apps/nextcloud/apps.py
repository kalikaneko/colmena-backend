from django.apps import AppConfig


class NctestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.nextcloud"
