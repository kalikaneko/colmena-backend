import os
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser
from rest_framework.exceptions import ParseError
from apps.nextcloud.resources import talk as nextcloud_talk_manager
from apps.nextcloud.resources import files as nextcloud_files_utils
from apps.nextcloud.resources import shares as nextcloud_shares_manager
from apps.nextcloud.resources import utils as nextcloud_utils
from apps.nextcloud import files as nextcloud_files_manager
from apps.nextcloud.config import get_talk_dir
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiExample
from apps.nextcloud.serializers import (
    NextcloudConversationSerializer,
    NextcloudConversationDetailsSerializer,
    NextcloudListSharedFilesResponseSerializer,
    NextcloudMessagesSerializer,
    NextcloudMessagesRequestSerializer,
    NextcloudListSharedFilesRequestSerializer,
    NextcloudSendMessageRequestSerializer,
    NextcloudShareFileRequestSerializer,
    NextcloudSendFileRequestSerializer,
    NextcloudUploadFileRequestSerializer,
)
from colmena.serializers.serializers import ErrorSerializer


class NextcloudConversationsAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NextcloudConversationSerializer

    @extend_schema(
        request=NextcloudConversationSerializer(),
        responses=NextcloudConversationSerializer(many=True),
    )
    def get(self, request):
        """
        Returns a list of conversations.
        """
        user_request = request.user
        try:
            nc_conversations = nextcloud_talk_manager.get_user_conversations(
                user=user_request
            )
            serializer = self.serializer_class(nc_conversations, many=True)
            return Response(serializer.data)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)


class NextcloudConversationDetailAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NextcloudConversationDetailsSerializer

    @extend_schema(
        responses=NextcloudConversationDetailsSerializer(),
        parameters=[
            OpenApiParameter(
                name="conversation_token",
                description="The conversation token for a nextcloud room",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Conversation token with ID b88v6yz2", value="b88v6yz2"
                    ),
                    OpenApiExample(
                        "Conversation token with ID bq7a32ak", value="bq7a32ak"
                    ),
                ],
            ),
        ],
    )
    def get(self, request, **kwargs):
        """
        Returns a conversation details.
        """
        user_request = request.user
        conversation_token = kwargs.get("conversation_token")

        try:
            nc_conversation = nextcloud_talk_manager.get_conversation(
                conversation_token=conversation_token, user=user_request
            )
            serializer = self.serializer_class(nc_conversation)
            return Response(serializer.data)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)


class NextcloudConversationFilesAPIView(APIView):
    parser_classes = [MultiPartParser]
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NextcloudListSharedFilesResponseSerializer

    @extend_schema(
        request=NextcloudListSharedFilesRequestSerializer,
        description="Returns all files and medias shared for the given conversation ",
        parameters=[
            OpenApiParameter(
                name="conversation_token",
                description="The conversation token for a nextcloud room",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Conversation token with ID b88v6yz2", value="b88v6yz2"
                    ),
                    OpenApiExample(
                        "Conversation token with ID bq7a32ak", value="bq7a32ak"
                    ),
                ],
            ),
            OpenApiParameter(
                name="limit",
                description="The max number of messages to return.",
                type=int,
                required=False,
                default=200,
            ),
            OpenApiParameter(
                name="descending",
                description="Whether to return items in ascending or descending order.",
                type=bool,
                required=False,
                default=True,
            ),
        ],
        examples=[
            OpenApiExample(
                "Response Example with limit 1",
                summary="Returns one file for the given conversation",
                response_only=True,
                value={
                    "files": [
                        {
                            "id": 1112,
                            "token": "3px44s3e",
                            "actorType": "users",
                            "actorId": "test",
                            "actorDisplayName": "test",
                            "timestamp": 1698941776,
                            "message": "{file}",
                            "messageParameters": {
                                "actor": {
                                    "type": "user",
                                    "id": "test",
                                    "name": "test",
                                },
                                "file": {
                                    "type": "file",
                                    "id": "1507",
                                    "name": "test.png",
                                    "size": 265370,
                                    "path": "test.png",
                                    "link": "https://some-url/f/1507",
                                    "etag": "7675a9eb16e72ff51ec0e9c182ec6b57",
                                    "permissions": 27,
                                    "mimetype": "image/png",
                                    "preview-available": "yes",
                                },
                            },
                            "systemMessage": "",
                            "messageType": "comment",
                            "isReplyable": True,
                            "referenceId": "2ac1ea706cdbf08f4bc48c1644575b8807e4ba7630002cec1713feab1b27d214",
                            "reactions": {},
                            "expirationTimestamp": 0,
                            "markdown": True,
                        }
                    ],
                    "quantity": 1,
                    "conversation_token": "3px44s3e",
                },
            ),
            OpenApiExample(
                "Response Example for an empty files list",
                summary="Returns an empty files list",
                response_only=True,
                value={"files": [], "quantity": 0, "conversation_token": "3px44s3e"},
            ),
        ],
    )
    def get(self, request, **kwargs):
        data = {
            "conversation_token": kwargs.get("conversation_token"),
        }

        if request.query_params.get("limit", False):
            data = data | {"limit": request.query_params.get("limit")}

        if request.query_params.get("descending", False):
            data = data | {"descending": request.query_params.get("descending")}

        request_serializer = NextcloudListSharedFilesRequestSerializer(
            data=data,
        )

        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_FILES_LIST_INVALID_REQUEST_PARAMS").data,
                status=400,
            )

        args = request_serializer.data | {"user": request.user}

        try:
            response = nextcloud_files_utils.list_conversation_files(**args)
            serializer = self.serializer_class(
                response, context={"conversation_token": args["conversation_token"]}
            )
            return Response(serializer.data)
        except Exception as e:
            return Response(
                ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data, status=404
            )

    @extend_schema(
        request=NextcloudSendFileRequestSerializer,
        description="Send a attached message for the given conversation_token",
        parameters=[
            OpenApiParameter(
                name="conversation_token",
                description="The conversation token for a nextcloud room",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Conversation token with ID b88v6yz2", value="b88v6yz2"
                    ),
                    OpenApiExample(
                        "Conversation token with ID bq7a32ak", value="bq7a32ak"
                    ),
                ],
            ),
        ],
    )
    def post(self, request, **kwargs):
        data = {
            "conversation_token": kwargs.get("conversation_token"),
            "file": request.data.get("file"),
            "filename": request.data.get("filename"),
        }
        request_serializer = NextcloudSendFileRequestSerializer(
            data=data,
        )
        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_REQUEST_ATTACHMENT_IS_INVALID").data, status=400
            )
        user = request.user
        new_file = request_serializer.validated_data.get("file")
        try:
            response = nextcloud_files_manager.send_as_attachment(
                new_file,
                request_serializer.validated_data.get("filename"),
                request_serializer.validated_data.get("conversation_token"),
                user,
            )
            return Response(response, status=200)
        except Exception as e:
            return Response(
                ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data, status=404
            )


class ConversationsMessagesAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NextcloudMessagesSerializer

    @extend_schema(
        request=NextcloudMessagesRequestSerializer,
        description="Returns all messages for the given conversation_token",
        parameters=[
            OpenApiParameter(
                name="conversation_token",
                description="The conversation token for a nextcloud room",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Conversation token with ID b88v6yz2", value="b88v6yz2"
                    ),
                    OpenApiExample(
                        "Conversation token with ID bq7a32ak", value="bq7a32ak"
                    ),
                ],
            ),
            OpenApiParameter(
                name="last_known_message",
                description="The id for a message to use as reference to fetch a list of messages",
                type=str,
                required=False,
                examples=[
                    OpenApiExample("Message with ID 10224", value="10224"),
                    OpenApiExample("Message with ID 10225", value="10225"),
                ],
            ),
            OpenApiParameter(
                name="look_into_future",
                description="Whether to fetch older or newer messages. Uses last_known_message as starting point when provided.",
                type=bool,
                required=False,
                default=True,
            ),
            OpenApiParameter(
                name="limit",
                description="The max number of messages to return.",
                type=int,
                required=False,
                default=200,
            ),
        ],
        examples=[
            OpenApiExample(
                "Example 1",
                summary="Returns a single message",
                value={
                    "messages": [
                        {
                            "id": 10413,
                            "token": "b88v6yz2",
                            "actorType": "users",
                            "actorId": "user1",
                            "actorDisplayName": "user1",
                            "timestamp": 1698332796,
                            "message": "{file}",
                            "messageParameters": {
                                "actor": {
                                    "type": "user",
                                    "id": "user1",
                                    "name": "user1",
                                },
                                "file": {
                                    "type": "file",
                                    "id": "12378",
                                    "name": "example.pdf",
                                    "size": 12033,
                                    "path": "Talk/example.pdf",
                                    "link": "https://some-url/f/12378",
                                    "etag": "3f32157f26da6faea1aba13d7d74b556",
                                    "permissions": 27,
                                    "mimetype": "application/pdf",
                                    "preview-available": "no",
                                },
                            },
                            "systemMessage": "",
                            "messageType": "comment",
                            "isReplyable": True,
                            "referenceId": "ce4aec0ac1ae37f2f25464b177bfc6eba74287ee475e440f5f982b184a54d0f3",
                            "reactions": {},
                            "expirationTimestamp": 0,
                        }
                    ],
                    "last_given": "10413",
                    "last_common_read": "10268",
                    "quantity": 1,
                    "conversation_token": "b88v6yz2",
                },
            )
        ],
    )
    def get(self, request, **kwargs):
        data = {
            "conversation_token": kwargs.get("conversation_token"),
        }
        if request.query_params.get("limit", False):
            data = data | {"limit": request.query_params.get("limit")}
        if request.query_params.get("last_known_message", False):
            data = data | {
                "last_known_message": request.query_params.get("last_known_message")
            }
        if request.query_params.get("look_into_future", False):
            data = data | {
                "look_into_future": request.query_params.get("look_into_future")
            }

        request_serializer = NextcloudMessagesRequestSerializer(
            data=data,
            context={"default_limit": 200},
        )
        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_FETCH_MESSAGES_FAILED").data, status=400
            )

        args = {
            **request_serializer.data,
            "user": request.user,
        }

        try:
            response = nextcloud_talk_manager.get_conversation_messages(**args)
            serializer = self.serializer_class(
                response,
                context={
                    "conversation_token": request_serializer.data["conversation_token"]
                },
            )
            return Response(serializer.data)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)

    @extend_schema(
        request=NextcloudSendMessageRequestSerializer,
        description="Send a message for the given conversation_token",
        parameters=[
            OpenApiParameter(
                name="conversation_token",
                description="The conversation token for a nextcloud room",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "Conversation token with ID b88v6yz2", value="b88v6yz2"
                    ),
                    OpenApiExample(
                        "Conversation token with ID bq7a32ak", value="bq7a32ak"
                    ),
                ],
            ),
        ],
        examples=[
            OpenApiExample(
                "Request Example 1",
                summary="Send a single message",
                request_only=True,
                value={"message": "Hello, world!"},
            ),
            OpenApiExample(
                "Response Example 1",
                summary="Send a single message",
                response_only=True,
                value={
                    "message": {
                        "id": 1110,
                        "token": "3px44s3e",
                        "actorType": "users",
                        "actorId": "test",
                        "actorDisplayName": "test",
                        "timestamp": 1698931475,
                        "message": "Hello, world!",
                        "messageParameters": [],
                        "systemMessage": "",
                        "messageType": "comment",
                        "isReplyable": True,
                        "referenceId": "",
                        "reactions": {},
                        "expirationTimestamp": 0,
                        "markdown": True,
                    },
                    "last_common_read": "1110",
                },
            ),
            OpenApiExample(
                "Request Example 2",
                summary="Reply a single message",
                request_only=True,
                value={"message": "Hello, Free Software!", "reply_to": 1110},
            ),
            OpenApiExample(
                "Response Example 2",
                summary="Reply a single message",
                response_only=True,
                value={
                    "message": {
                        "id": 1111,
                        "token": "3px44s3e",
                        "actorType": "users",
                        "actorId": "test",
                        "actorDisplayName": "test",
                        "timestamp": 1698938841,
                        "message": "Hello, Free Software!",
                        "messageParameters": [],
                        "systemMessage": "",
                        "messageType": "comment",
                        "isReplyable": True,
                        "referenceId": "",
                        "reactions": {},
                        "expirationTimestamp": 0,
                        "markdown": True,
                        "parent": {
                            "id": 1110,
                            "token": "3px44s3e",
                            "actorType": "users",
                            "actorId": "test",
                            "actorDisplayName": "test",
                            "timestamp": 1698931475,
                            "message": "Hello, world!",
                            "messageParameters": [],
                            "systemMessage": "",
                            "messageType": "comment",
                            "isReplyable": True,
                            "referenceId": "",
                            "reactions": {},
                            "expirationTimestamp": 0,
                            "markdown": True,
                        },
                    },
                    "last_common_read": "1111",
                },
            ),
        ],
    )
    def post(self, request, **kwargs):
        request_serializer = NextcloudSendMessageRequestSerializer(
            data={
                **request.data,
                "conversation_token": kwargs.get("conversation_token"),
            },
        )

        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_SEND_MESSAGE_FAILED").data, status=400
            )

        args = request_serializer.data | {"user": request.user}

        try:
            response = nextcloud_talk_manager.send_message_to_conversation(**args)
            return Response(response)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)


class NextcloudUploadFileAPIView(APIView):
    parser_classes = [MultiPartParser]
    permission_classes = [IsAuthenticated]
    authentication_classes = [JWTAuthentication]
    serializer_class = NextcloudUploadFileRequestSerializer

    @extend_schema(
        request=NextcloudUploadFileRequestSerializer,
        parameters=[
            OpenApiParameter(
                name="filename",
                description="A name for the file",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample(
                        "File with name hello-world.jpg", value="hello-world.jpg"
                    ),
                    OpenApiExample("File with name free.pdf", value="free.pdf"),
                ],
            ),
        ],
    )
    def post(self, request, **kwargs):
        filename = kwargs.get("filename")
        if filename is None or filename == "":
            return Response("ERRORS_NEXTCLOUD_FILENAME_IS_MISSING", status=400)

        file = request.data["file"]
        try:
            remote_path = nextcloud_files_manager.upload_file(file, request.user)
            return Response(remote_path, status=200)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)


class NextcloudShareFileConversationAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NextcloudShareFileRequestSerializer

    @extend_schema(
        request=NextcloudShareFileRequestSerializer,
        description="Send a message for the given conversation_token",
        parameters=[
            OpenApiParameter(
                name="group_id",
                description="The group id in nextcloud group",
                location=OpenApiParameter.PATH,
                type=str,
                required=True,
                examples=[
                    OpenApiExample("Group with ID b88v6yz2", value="b88v6yz2"),
                    OpenApiExample("Group with ID bq7a32ak", value="bq7a32ak"),
                ],
            ),
        ],
    )
    def post(self, request, **kwargs):
        request_serializer = NextcloudShareFileRequestSerializer(
            data={
                **request.data,
            },
        )

        if not request_serializer.is_valid():
            # TODO: Find a way to send the serializer error dictionary, in addition to the error message.
            return Response(
                ErrorSerializer("ERRORS_SHARE_FILE_FAILED").data, status=400
            )

        args = request_serializer.data | {"user": request.user}

        try:
            response = nextcloud_shares_manager.create_conversation_share(**args)
            return Response(response)
        except Exception as e:
            return Response(ErrorSerializer("ERRORS_NEXTCLOUD_SERVER_ERROR").data)
