import asyncio
from django.core.exceptions import ValidationError, ImproperlyConfigured, BadRequest
from apps.nextcloud.resources.auth import auth
from apps.nextcloud import occ
from nextcloud_async import exceptions as nextcloud_exceptions
from colmena.settings.base import NEXTCLOUD_DEFAULT_USER_QUOTA

from django.conf import settings


def create_user(
    user_id=None,
    display_name=None,
    user_email=None,
    password=None,
    auth_user_id=None,
    auth_user_app_password=None,
):
    # WTF?-- this will never authenticate, you want to be the superadmin here don't you???
    # nextcloudAsync = auth(auth_user_id, auth_user_app_password)
    nextcloudAsync = auth(
        settings.NEXTCLOUD_ADMIN_USER, settings.NEXTCLOUD_ADMIN_PASSWORD
    )
    try:
        asyncio.run(
            nextcloudAsync.create_user(
                user_id=user_id,
                display_name=display_name,
                email=user_email,
                quota=NEXTCLOUD_DEFAULT_USER_QUOTA,
                ##FIXME : A default value is used because there is no real impact at this time.
                language="en",
                password=password,
            )
        )
    except nextcloud_exceptions.NextCloudException as e:
        print("==> exception: ", e)
        raise BadRequest("ERRORS_NEXTCLOUD_USER_CREATION_ERROR")

    return True


def get_user(user_id=None, auth_user_id=None, auth_user_app_password=None):
    nextcloudAsync = auth(
        settings.NEXTCLOUD_ADMIN_USER, settings.NEXTCLOUD_ADMIN_PASSWORD
    )
    try:
        user = asyncio.run(nextcloudAsync.get_user(user_id))

    except nextcloud_exceptions.NextCloudException as e:
        ## FIXME: Save the error in logs
        raise BadRequest("ERRORS_NEXTCLOUD_USER_NOT_FOUND")

    return user


def delete_user(user_id=None, auth_user_id=None, auth_user_app_password=None):
    nextcloudAsync = auth(
        settings.NEXTCLOUD_ADMIN_USER, settings.NEXTCLOUD_ADMIN_PASSWORD
    )
    try:
        asyncio.run(nextcloudAsync.remove_user(user_id))
    except nextcloud_exceptions.NextCloudException as e:
        ## FIXME: Save the error in logs
        raise BadRequest("ERRORS_NEXTCLOUD_USER_DELETION_FAILED")


def create_app_password(user_id=None, user_password=None):
    try:
        return occ.create_app_password(user_id, user_password)
    except nextcloud_exceptions.NextCloudException as e:
        # FIXME: Save the error in logs
        return None


def user_exists_in_nextcloud(user_id, auth_user_id, auth_user_app_password):
    try:
        return (
            user_id
            == get_user(
                user_id=user_id,
                auth_user_id=auth_user_id,
                auth_user_app_password=auth_user_app_password,
            )["id"]
        )

    except Exception as e:
        raise BadRequest("ERRORS_NEXTCLOUD_GET_USER_FAILED")
