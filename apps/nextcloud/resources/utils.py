import os
from colmena.serializers.serializers import ErrorSerializer
from django.core.exceptions import ValidationError
from django.conf import settings
from colmena.settings.base import SUPERADMIN_EMAIL


def validate_group_id(group_id):
    if not group_id:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_ID_IS_BLANK").data
        )


def validate_user_id(user_id):
    if not user_id:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_USER_ID_IS_BLANK").data)


def validate_not_admin_group(group_id):
    if group_id == "admin":
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GROUP_EXIST").data)


def validate_conversation_token(token):
    if not token:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_ID_IS_BLANK").data
        )


def validate_message_id(message_id):
    if not message_id and not isinstance(las, str):
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_MESSAGE_ID_IS_INVALID").data
        )


def get_conversation_token(conversation=None):
    try:
        return conversation["token"]
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_TOKEN").data)


def validate_filter_value(value=None):
    if not value:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILTER_VALUE_IS_BLANK").data
        )


def validate_room_name(room_name=None):
    if not room_name:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_ROOM_NAME_IS_BLANK").data
        )


def extract_organization_pk_to_group_id(group_id):
    substrings = string.split("-")
    return substrings[1]


def validate_user(user=None):
    pass


def handle_uploaded_file(f):
    abs_file = os.path.join(settings.MEDIA_ROOT, f.name)
    with open(abs_file, "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return abs_file
