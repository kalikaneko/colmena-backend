import asyncio
import os
from nextcloud_async import exceptions as nextcloud_exceptions
from apps.nextcloud.resources.auth import auth
from django.core.exceptions import ValidationError
from colmena.serializers.serializers import ErrorSerializer
from apps.nextcloud.config import get_talk_dir
from apps.nextcloud.resources import utils as nextcloud_util
from apps.nextcloud.resources import talk as nextcloud_talks
from apps.nextcloud.resources import groups as nextcloud_groups
from nextcloud_async.api.ocs.shares import SharePermission, ShareType


def create_conversation_share(file_path, conversation_token, user):
    """
    Given a file_path, a conversation_token and a user to authenticate the request,
    sends a shared file in the conversation associated to the given conversation token.

    Parameters
    ----------
    file_path : str
        The nextcloud internal path

    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        "id": String,
        "share_type": Enum,
        "uid_owner": String,
        "displayname_owner": String,
        "permissions": Enum,
        "can_edit": Boolean,
        "can_delete": Boolean,
        "stime": Integer,
        "parent": Object,
        "expiration": Date,
        "token": None,
        "uid_file_owner": String,
        "note": String,
        "label": String,
        "displayname_file_owner": String,
        "path": String,
        "item_type": String,
        "mimetype": String,
        "has_preview": True,
        "storage_id": String,
        "storage": Integer,
        "item_source": Integer,
        "file_source": Integer,
        "file_parent": Integer,
        "file_target": String,
        "share_with": String,
        "share_with_displayname": String,
        "share_with_link": String,
        "mail_send": Integer,
        "hide_download": Integer,
        "attributes": None,
    }

    Raises
    ------
    ValidationError
        When either the conversation_token or the file_path don't exist in nextcloud.
    """

    nextcloud_util.validate_conversation_token(conversation_token)
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        return asyncio.run(
            nextcloud_async.create_share(
                path=file_path,
                share_type=ShareType.room,
                permissions=SharePermission.all,
                share_with=conversation_token,
            )
        )
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_SEND_SHARE_ERROR").data)


def get_share(share_id, user):
    """
    Given a share_id and a user to authenticate the request, returns
    the shared data to the given share_id.

    Parameters
    ----------
    share_id : int
        The share id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    {
        "id": String,
        "share_type": Enum,
        "uid_owner": String,
        "displayname_owner": String,
        "permissions": Enum,
        "can_edit": Boolean,
        "can_delete": Boolean,
        "stime": Integer,
        "parent": Object,
        "expiration": Date,
        "token": None,
        "uid_file_owner": String,
        "note": String,
        "label": String,
        "displayname_file_owner": String,
        "path": String,
        "item_type": String,
        "mimetype": String,
        "has_preview": True,
        "storage_id": String,
        "storage": Integer,
        "item_source": Integer,
        "file_source": Integer,
        "file_parent": Integer,
        "file_target": String,
        "share_with": String,
        "share_with_displayname": String,
        "share_with_link": String,
        "mail_send": Integer,
        "hide_download": Integer,
        "attributes": None,
    }

    Raises
    ------
    ValidationError
        When conversation_token do not exist
    """
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        return asyncio.run(nextcloud_async.get_share(share_id))
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GET_SHARE_ERROR").data)


def get_all_shares(user):
    """
    Given a user to authenticate the request, returns all shares for the
    user or raises.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    [
        {
            "id": String,
            "share_type": Enum,
            "uid_owner": String,
            "displayname_owner": String,
            "permissions": Enum,
            "can_edit": Boolean,
            "can_delete": Boolean,
            "stime": Integer,
            "parent": Object,
            "expiration": Date,
            "token": None,
            "uid_file_owner": String,
            "note": String,
            "label": String,
            "displayname_file_owner": String,
            "path": String,
            "item_type": String,
            "mimetype": String,
            "has_preview": True,
            "storage_id": String,
            "storage": Integer,
            "item_source": Integer,
            "file_source": Integer,
            "file_parent": Integer,
            "file_target": String,
            "share_with": String,
            "share_with_displayname": String,
            "share_with_link": String,
            "mail_send": Integer,
            "hide_download": Integer,
            "attributes": None,
        }
    ]

    Raises
    ------
    ValidationError
        When user does not exist
    """

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        return asyncio.run(nextcloud_async.get_all_shares())
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GET_ALL_ERROR").data)
