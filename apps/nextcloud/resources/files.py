import asyncio
import os

from django.core.exceptions import ValidationError
from django.conf import settings
from nextcloud_async import exceptions as nextcloud_exceptions

from apps.nextcloud.resources.auth import auth
from apps.nextcloud.config import get_talk_dir
from apps.nextcloud.resources import utils as nextcloud_util
from colmena.serializers.serializers import ErrorSerializer

TALK_DIR = get_talk_dir()


def list_files(user=None):
    """
    Given a user to authenticate the request, returns all nextcloud files for the given user.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        [
            {
                "d:href": String,
                "d:propstat": [
                    {
                        "d:prop": {
                            "nc:sharees": {
                                "nc:sharee": {
                                    "nc:id": String,
                                    "nc:display-name": String,
                                    "nc:type": Enum,
                                }
                            },
                            "oc:share-types": {"oc:share-type": Enum},
                            "d:getlastmodified": Date,
                            "d:getcontentlength": String,
                            "d:resourcetype": Enum,
                            "d:getetag": String,
                            "d:getcontenttype": Enum,
                        },
                        "d:status": Enum,
                    },
                    {"d:prop": {"d:displayName": String}, "d:status": String}
                ]
            }
        ]


    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """
    props = [
        "d:displayName",
        "nc:sharees",
        "oc:share-types",
        "d:getlastmodified",
        "d:getcontentlength",
        "d:resourcetype",
        "d:getetag",
        "d:getcontenttype",
    ]
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        response = asyncio.run(nextcloud_async.list_files(TALK_DIR, props))

        # Handles the case where there is only one item to be added to a list.
        if isinstance(response, dict):
            response = [response]

        return response
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_GET_FILES_FAILED").data)


def list_conversation_files(
    conversation_token=None, user=None, limit=100, descending=True
):
    """
    Given a user to authenticate the request, returns the last 100 files and media attached to the given conversation in descending order.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        {
            "audio": [],
            "deckcard": [],
            "file": [
                {
                    "id": Number,
                    "token": String,
                    "actorType": Enum,
                    "actorId": String,
                    "actorDisplayName": String,
                    "timestamp": timestamp,
                    "message": String,
                    "messageParameters": {
                        "actor": {
                            "type": String,
                            "id": String,
                            "name": String
                        },
                        "file": {
                            "type": "file",
                            "id": String,
                            "name": String,
                            "size": Number,
                            "path": String,
                            "link": String,
                            "etag": String,
                            "permissions": Enum,
                            "mimetype": Enum,
                            "preview-available": String
                        }
                    },
                    "systemMessage": String,
                    "messageType":Enum,
                    "isReplyable": Bool,
                    "referenceId": String,
                    "reactions": Dict,
                    "expirationTimestamp": Number
                },
            ],
            "location": [],
            "media": [],
            "other": [],
            "poll": [],
            "voice": [],
        }

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    nextcloud_util.validate_conversation_token(conversation_token)
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        response = asyncio.run(
            # The limit is hardcoded to the last 100 elements of each type.
            nextcloud_async.get_shared_items_overview(conversation_token, limit=100)
        )
        # Concatenate files and media elements and sort by timestamp in ascending or descending order
        data = {
            "files": sorted(
                response["file"] + response["media"],
                key=lambda x: x["timestamp"],
                reverse=descending,
            )[:limit]
        }
        return data
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_CONVERSATION_FILES_FAILED").data
        )


def create_talk_folder(user):
    """
    Given a user to authenticate the request, creates a default talk folder.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
       None

    Raises
    ------
    ValidationError
        When the nextcloud server answers unexpectedly.
    """
    # nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    # nextcloud_async = auth(settings.NEXTCLOUD_ADMIN_USER, settings.NEXTCLOUD_ADMIN_PASSWORD)
    print("==> user", user.get_nextcloud_user_id())
    print("==> pass", user.nc_app_password)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(nextcloud_async.create_folder(TALK_DIR))
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CREATION_FOLDER_FAILED").data
        )


def upload(local_path, filename, user):
    """
    Given a local path, a filename and a user to authenticate the request,
    uploads a file in the local path to the default Talk folder.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    remote_path = os.path.join(TALK_DIR, filename)
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(
            nextcloud_async.upload_file(local_path=local_path, remote_path=remote_path)
        )
    except FileNotFoundError as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_FILE_NOT_FOUND").data)
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FILE_UPLOAD_FAILED").data
        )


def delete_file(file_path, user):
    """
    Given a file path and a user to authenticate the request removes the file

    Parameters
    ----------

    file_path : str
       A file path

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        When the conversation_token is not a valid value.
    """
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        asyncio.run(nextcloud_async.delete(path=file_path))

    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(ErrorSerializer("ERRORS_NEXTCLOUD_DELETION_ERROR").data)
