import asyncio
from apps.nextcloud.resources.auth import auth
from nextcloud_async import exceptions as nextcloud_exceptions
from django.core.exceptions import ValidationError
from colmena.serializers.serializers import ErrorSerializer
from apps.nextcloud.resources import utils as nextcloud_utils
from nextcloud_async.api.ocs.talk import ConversationType


def create_group_conversation(room_name=None, group_id=None, user=None):
    """
    Given a group_id, a room_name and a user to authenticate the request,
    creates a new group conversation in nextcloud with the given room_name and
    invites the group.

    Parameters
    ----------
    room_name : str
        The conversation name

    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        {
            "id": Number,
            "token": String,
            "type": Enum,
            "name": String,
            "displayName": String,
            "objectType": String,
            "objectId": String,
            "participantType": Enum,
            "participantFlags": Number,
            "readOnly": Number,
            "hasPassword": Boolean,
            "hasCall": Boolean,
            "canStartCall": Boolean,
            "lastActivity": Timestamp,
            "lastReadMessage": Timestamp,
            "unreadMessages": Number,
            "unreadMention": Boolean,
            "unreadMentionDirect": Boolean,
            "isFavorite": Boolean,
            "canLeaveConversation": Boolean,
            "canDeleteConversation": Boolean,
            "notificationLevel": Number,
            "notificationCalls": Number,
            "lobbyState": Number,
            "lobbyTimer": Number,
            "lastPing": Number,
            "sessionId": String,
            "lastMessage": Message,
            "sipEnabled": Number,
            "actorType": Enum,
            "actorId": String,
            "attendeeId": Number,
            "permissions": Number,
            "attendeePermissions": Number,
            "callPermissions": Number,
            "defaultPermissions": Number,
            "canEnableSIP": Boolean,
            "attendeePin": String,
            "description": String,
            "lastCommonReadMessage": Number,
            "listable": Number,
            "callFlag": Number,
            "messageExpiration": Number,
        }

    Raises
    ------
    ValidationError
        When either group_id do not exist
    """
    nextcloud_utils.validate_group_id(group_id)
    nextcloud_utils.validate_room_name(room_name)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        conversation = asyncio.run(
            nextcloud_async.create_conversation(
                room_type=ConversationType.group.name,
                invite=group_id,
                room_name=room_name,
            )
        )
        return conversation
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_CREATION_FAILED").data
        )


def get_conversation(conversation_token=None, user=None):
    """
    Given a conversation_token and a user to authenticate the request, returns
    the conversation associated to the given conversation token.


    Parameters
    ----------
    conversation_token : str
        The conversation token


    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        {
            "id": Number,
            "token": String,
            "type": Enum,
            "name": String,
            "displayName": String,
            "objectType": String,
            "objectId": String,
            "participantType": Enum,
            "participantFlags": Number,
            "readOnly": Number,
            "hasPassword": Boolean,
            "hasCall": Boolean,
            "canStartCall": Boolean,
            "lastActivity": Timestamp,
            "lastReadMessage": Timestamp,
            "unreadMessages": Number,
            "unreadMention": Boolean,
            "unreadMentionDirect": Boolean,
            "isFavorite": Boolean,
            "canLeaveConversation": Boolean,
            "canDeleteConversation": Boolean,
            "notificationLevel": Number,
            "notificationCalls": Number,
            "lobbyState": Number,
            "lobbyTimer": Number,
            "lastPing": Number,
            "sessionId": String,
            "lastMessage": Message,
            "sipEnabled": Number,
            "actorType": Enum,
            "actorId": String,
            "attendeeId": Number,
            "permissions": Number,
            "attendeePermissions": Number,
            "callPermissions": Number,
            "defaultPermissions": Number,
            "canEnableSIP": Boolean,
            "attendeePin": String,
            "description": String,
            "lastCommonReadMessage": Number,
            "listable": Number,
            "callFlag": Number,
            "messageExpiration": Number,
        }

    Raises
    ------
    ValidationError
        When conversation_token do not exist
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        return asyncio.run(nextcloud_async.get_conversation(conversation_token))
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_NOT_FOUND").data
        )


def get_conversation_participants(conversation_token=None, user=None):
    """
    Given a conversation_token and a user to authenticate the request, returns
    the list of users associated to the given conversation token.


    Parameters
    ----------
    conversation_token : str
        The conversation token


    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
    [
        {
            'inCall': Number,
            'lastPing': Number,
            'sessionIds': [SessionID],
            'participantType': Number,
            'attendeeId': Number,
            'actorId': String,
            'actorType': Enum,
            'displayName': String,
            'permissions': Number,
            'attendeePermissions': Number,
            'attendeePin': String,
            'status': String,
            'statusIcon': String,
            'statusMessage': String,
            'statusClearAt': Timestamp
        }
    ]

    Raises
    ------
    ValidationError
        When conversation_token do not exist
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        return asyncio.run(
            nextcloud_async.get_conversation_participants(conversation_token)
        )
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_USERS_CONVERSATION_NOT_FOUND").data
        )


def get_user_conversations(user=None):
    """
    Given a user to authenticate the request, returns all conversations for the
    user or raises.

    Parameters
    ----------

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        List of Conversation:
        [
            {
                "id": Number,
                "token": String,
                "type": Enum,
                "name": String,
                "displayName": String,
                "objectType": String,
                "objectId": String,
                "participantType": Enum,
                "participantFlags": Number,
                "readOnly": Number,
                "hasPassword": Boolean,
                "hasCall": Boolean,
                "canStartCall": Boolean,
                "lastActivity": Timestamp,
                "lastReadMessage": Timestamp,
                "unreadMessages": Number,
                "unreadMention": Boolean,
                "unreadMentionDirect": Boolean,
                "isFavorite": Boolean,
                "canLeaveConversation": Boolean,
                "canDeleteConversation": Boolean,
                "notificationLevel": Number,
                "notificationCalls": Number,
                "lobbyState": Number,
                "lobbyTimer": Number,
                "lastPing": Number,
                "sessionId": String,
                "lastMessage": Message,
                "sipEnabled": Number,
                "actorType": Enum,
                "actorId": String,
                "attendeeId": Number,
                "permissions": Number,
                "attendeePermissions": Number,
                "callPermissions": Number,
                "defaultPermissions": Number,
                "canEnableSIP": Boolean,
                "attendeePin": String,
                "description": String,
                "lastCommonReadMessage": Number,
                "listable": Number,
                "callFlag": Number,
                "messageExpiration": Number
            }
        ]

    Raises
    ------
    ValidationError
        When user does not exist
    """
    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)
    try:
        return asyncio.run(nextcloud_async.get_conversations())
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_FETCH_USER_CONVERSATIONS_FAILED").data
        )


def search_user_conversation(filter_key="name", filter_value=None, user=None):
    """
    Given a filter_key, a filter_value and a user to authenticate the request, returns a conversation that matches
    the filter_value at the given filter_key, or None when no value matches.

    Parameters
    ----------
    group_id : str
        The nextcloud group id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
       {
            "id": Number,
            "token": String,
            "type": Enum,
            "name": String,
            "displayName": String,
            "objectType": String,
            "objectId": String,
            "participantType": Enum,
            "participantFlags": Number,
            "readOnly": Number,
            "hasPassword": Boolean,
            "hasCall": Boolean,
            "canStartCall": Boolean,
            "lastActivity": Timestamp,
            "lastReadMessage": Timestamp,
            "unreadMessages": Number,
            "unreadMention": Boolean,
            "unreadMentionDirect": Boolean,
            "isFavorite": Boolean,
            "canLeaveConversation": Boolean,
            "canDeleteConversation": Boolean,
            "notificationLevel": Number,
            "notificationCalls": Number,
            "lobbyState": Number,
            "lobbyTimer": Number,
            "lastPing": Number,
            "sessionId": String,
            "lastMessage": Message,
            "sipEnabled": Number,
            "actorType": Enum,
            "actorId": String,
            "attendeeId": Number,
            "permissions": Number,
            "attendeePermissions": Number,
            "callPermissions": Number,
            "defaultPermissions": Number,
            "canEnableSIP": Boolean,
            "attendeePin": String,
            "description": String,
            "lastCommonReadMessage": Number,
            "listable": Number,
            "callFlag": Number,
            "messageExpiration": Number,
        }

    Raises
    ------
    ValidationError
        If the conversation cannot be retrieved
    """
    nextcloud_utils.validate_filter_value(filter_value)

    try:
        # Obtain a list of conversation
        conversations = get_user_conversations(user=user)
        # Filter to specific field and value
        conversation = [
            conversation
            for conversation in conversations
            if conversation.get(filter_key) == filter_value
        ]
        if conversation:
            return conversation[0]
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_NOT_FOUND").data
        )


def remove_conversation(conversation_token=None, user=None):
    """
    Given a conversation_token and a user to authenticate the request, removes a
    conversation in nextcloud.

    Parameters
    ----------
    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the group_id does not exist
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(nextcloud_async.remove_conversations(token=conversation_token))
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_REMOVAL_FAILED").data
        )


def invite_user_to_conversation(conversation_token=None, user_id=None, user=None):
    """
    Given a conversation_token, a user_id and a user to authenticate the request, adds the user to the
    nextcloud conversation.

    Parameters
    ----------
    conversation_token : str
        The conversation token

    user_id : str
        The user id

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password.

    Returns
    -------
        None

    Raises
    ------
    ValidationError
        If the conversation_token or user_id does not exist
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    nextcloud_utils.validate_user_id(user_id)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        asyncio.run(
            nextcloud_async.invite_to_conversation(
                token=conversation_token, invitee=user_id
            )
        )
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_REMOVAL_FAILED").data
        )


def get_conversation_messages(
    conversation_token=None,
    user=None,
    last_known_message=None,
    look_into_future=True,
    limit=200,
):
    """
    Given a conversation_token and a user to authenticate the request, returns the last 200 messages of a
    room conversation in ascending order.

    Parameters
    ----------
    conversation_token : str
        The conversation token

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password
        and this user is included in the conversation.

    last_known_message: int
        A message id to be used as an index reference to fetch older or new messages.

    look_into_future : Bool [optional, default=False]
        Decides whether to fetch older or newer messages. Messages are returned
        in descending order when False and ascending order when True

    limit : int [optional, default=200]
        The max number of messages returned.


    Returns
    -------
    {
        "messages": [
            {
                "id": Number,
                "token": String,
                "actorType": Enum,
                "actorId": String,
                "actorDisplayName": String,
                "timestamp": Number,
                "systemMessage": String,
                "messageType": String,
                "isReplyable": Boolean,
                "referenceId": String,
                "message": String,
                "messageParameters": Array,
                "parent": Array, // [Optional]
                "reactions": Array,
                "reactionsSelf": Array, // [Optional]
                "expirationTimestamp": Number
            }
        ],
        "last_given": String,
        "last_common_read": String,
    }

    Raises
    ------
    ValidationError
       If the user does not exist or does not belong to the conversation
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    args = {
        "token": conversation_token,
        "limit": limit,
    }
    if last_known_message:
        args = args | {"last_known_message": last_known_message}

    if look_into_future:
        args = args | {"look_into_future": look_into_future}

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        response = asyncio.run(nextcloud_async.get_conversation_messages(**args))
        # Extract data to response tuple and build a response dict
        data = {
            "messages": response[0],
            "last_given": response[1]["X-Chat-Last-Given"],
            "last_common_read": response[1]["X-Chat-Last-Common-Read"],
        }

        return data

    except nextcloud_exceptions.NextCloudRequestTimeout as e:
        last_given = None

        if last_known_message:
            last_given = last_known_message

        data = {
            "messages": [],
            "last_given": last_given,
            "last_common_read": None,
        }

        return data

    except nextcloud_exceptions.NextCloudNotFound as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_CONVERSATION_NOT_FOUND").data
        )
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_GET_ROOM_MESSAGES_FAILED").data
        )


def send_message_to_conversation(
    conversation_token=None, message=None, reply_to=0, user=None
):
    """
    Given a conversation_token, a message and the user to authenticate the request, sends a message to the
    nextcloud conversation.

    Parameters
    ----------
    conversation_token : str
        The conversation token

    message : str
        The message content (limit=32000 character)

    reply_to : int [optional, default=0]
        The message id to reply to

    user : User
        A user model. This user is assumed to have the required
        privileges to perform nextcloud requests with an app password
        and this user is included in the conversation.

    Returns
    -------
     {
        "messages":
            {
                "id": Number,
                "token": String,
                "actorType": Enum,
                "actorId": String,
                "actorDisplayName": String,
                "timestamp": Number,
                "systemMessage": String,
                "messageType": String,
                "isReplyable": Boolean,
                "referenceId": String,
                "message": String,
                "messageParameters": Array,
                "parent": Array, // [Optional]
                "reactions": Array,
                "reactionsSelf": Array, // [Optional]
                "expirationTimestamp": Number
            }
        ,
        "last_common_read": String,
    }

    Raises
    ------
    ValidationError
        If the user does not exist or does not belong to the conversation.
    """
    nextcloud_utils.validate_conversation_token(conversation_token)

    nextcloud_async = auth(user.get_nextcloud_user_id(), user.nc_app_password)

    try:
        response = asyncio.run(
            nextcloud_async.send_to_conversation(
                token=conversation_token, message=message, reply_to=reply_to
            )
        )
        data = {
            "message": response[0],
            "last_common_read": response[1]["X-Chat-Last-Common-Read"],
        }

        return data

    except nextcloud_exceptions.NextCloudBadRequest as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_SEND_MESSAGE_FAILED").data
        )
    except nextcloud_exceptions.NextCloudNotFound as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_USER_NOT_INCLUDED_IN_CONVERSATION").data
        )
    except nextcloud_exceptions.NextCloudException as e:
        raise ValidationError(
            ErrorSerializer("ERRORS_NEXTCLOUD_SEND_MESSAGE_FAILED").data
        )
