import httpx
from django.core.exceptions import ValidationError, ImproperlyConfigured
from nextcloud_async import NextCloudAsync
from colmena.settings.base import ASYNC_CLIENT_TIMEOUT
from apps.nextcloud.config import get_nextcloud_api_url


# RESULT: It seems that we can un cache pickling the object because is async
def auth(user_id, user_app_password):
    if not user_app_password:
        raise ValidationError("ERRORS_NEXTCLOUD_APP_PASSWORD_IS_BLANK")

    endpoint = get_nextcloud_api_url()
    if endpoint == "":
        raise ImproperlyConfigured("nextcloud_api_url is empty")
    print("==> endpoint:", endpoint)

    nca = NextCloudAsync(
        client=httpx.AsyncClient(timeout=ASYNC_CLIENT_TIMEOUT),
        endpoint=endpoint,
        user=user_id,
        password=user_app_password,
    )

    return nca
