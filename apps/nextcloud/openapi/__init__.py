from apps.nextcloud.config import get_nextcloud_api_wrapper_url
from .client.services import app_password_service, healthcheck_service
from .client.models import AppPasswordIn
from .client.api_config import APIConfig


def _api_config():
    return APIConfig(base_path=get_nextcloud_api_wrapper_url())


def send_healthcheck_request():
    return healthcheck_service.get_healthcheck(_api_config())


def send_app_password_request(username, password):
    return app_password_service.create_app_password(
        AppPasswordIn(username=username, password=password), _api_config()
    )
