import os
from rest_framework import exceptions
from rest_framework import serializers
from apps.nextcloud import files as nextcloud_files_manager
from apps.organizations import serializers as organization_serializers

MAX_CHARACTERS_IN_MESSAGE = 32000


class NextcloudLastMessageSerializer(serializers.Serializer):
    last_message_id = serializers.IntegerField(source="id")
    last_message_text = serializers.CharField(source="message")
    last_message_timestamp = serializers.IntegerField(source="timestamp")
    last_messages_parameters = serializers.SerializerMethodField()

    def get_last_messages_parameters(self, obj):
        value = obj["messageParameters"]
        if isinstance(value, list):
            value = {}
        return value


class NextcloudLastConversationMessageSerializer(serializers.Serializer):
    conversation_token = serializers.CharField(source="token")
    last_message = NextcloudLastMessageSerializer(source="lastMessage")


class NextcloudConversationSerializer(NextcloudLastConversationMessageSerializer):
    conversation_name = serializers.CharField(source="name")


class NextcloudConversationTokenSerializer(serializers.Serializer):
    conversation_token = serializers.CharField(required=True)


class NextcloudListSharedFilesRequestSerializer(NextcloudConversationTokenSerializer):
    limit = serializers.IntegerField(required=False, default=200)
    descending = serializers.BooleanField(required=False, default=True)


class NextcloudConversationDetailsSerializer(NextcloudConversationSerializer):
    last_activity = serializers.IntegerField(source="lastActivity")


class NextcloudListSharedFilesResponseSerializer(
    organization_serializers.TeamListSharedFilesResponseSerializer
):
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["conversation_token"] = self.context.get("conversation_token")
        return representation


class NextcloudMessagesSerializer(serializers.Serializer):
    messages = serializers.ListField(child=serializers.DictField())
    last_given = serializers.CharField()
    last_common_read = serializers.CharField()

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["quantity"] = len(representation["messages"])
        representation["conversation_token"] = self.context.get("conversation_token")

        return representation


class NextcloudMessagesRequestSerializer(NextcloudConversationTokenSerializer):
    limit = serializers.IntegerField(required=False)
    look_into_future = serializers.BooleanField(required=False, default=True)
    last_known_message = serializers.CharField(required=False)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if not "limit" in representation.keys():
            representation["limit"] = self.context.get("default_limit")
        return representation


class NextcloudSendMessageRequestSerializer(serializers.Serializer):
    conversation_token = serializers.CharField(required=True)
    message = serializers.CharField(max_length=MAX_CHARACTERS_IN_MESSAGE, required=True)
    reply_to = serializers.IntegerField(required=False, default=0)


class NextcloudUploadFileRequestSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)
    filename = serializers.CharField(required=True)

    def validate_filename(self, value):
        _, extension = os.path.splitext(value)
        extension = extension.lower()
        if nextcloud_files_manager.is_valid_extension(extension):
            return value
        else:
            raise serializers.ValidationError("ERRORS_EXTENSION_IS_NOT_VALID")


class NextcloudSendFileRequestSerializer(
    NextcloudConversationTokenSerializer, NextcloudUploadFileRequestSerializer
):
    pass


class NextcloudShareFileRequestSerializer(NextcloudConversationTokenSerializer):
    file_path = serializers.CharField(required=True)
