import json
from django.core.management import call_command
from .openapi.client.api_config import HTTPException
from .openapi import send_app_password_request


def create_app_password(nc_username, nc_password):
    """
    Given a nextcloud username and a password requests an app password
    creation for the username provided and returns the generated value.

    If the request is successful the raw value is returned.

    Parameters
    ----------
    nc_username : str
        The username of a nextcloud user

    nc_password : str
        The username password

    Raises
    ------
    AppPasswordException
        If the connection can't be established or credentials are not valid.

    """
    try:
        response = send_app_password_request(username=nc_username, password=nc_password)
        return response.app_password

    except HTTPException as e:
        msg = json.loads(e.body)["message"]
        raise AppPasswordException(message=msg)


class AppPasswordException(Exception):
    """
    Raised when the app password could not be created.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
