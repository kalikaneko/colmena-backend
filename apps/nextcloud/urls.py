from . import views
from django.urls import path, re_path

urlpatterns = [
    path(
        "conversations",
        views.NextcloudConversationsAPIView.as_view(),
        name="list_conversations",
    ),
    path(
        "conversations/<conversation_token>/files",
        views.NextcloudConversationFilesAPIView.as_view(),
        name="conversation_files",
    ),
    path(
        "conversations/<conversation_token>",
        views.NextcloudConversationDetailAPIView.as_view(),
        name="get_conversation",
    ),
    path(
        "conversations/<conversation_token>/messages",
        views.ConversationsMessagesAPIView().as_view(),
        name="conversation_messages",
    ),
    path(
        "shares/<group_id>",
        views.NextcloudShareFileConversationAPIView.as_view(),
        name="shares",
    ),
    path(
        "files/<filename>",
        views.NextcloudUploadFileAPIView.as_view(),
        name="files",
    ),
]
