from apps.nextcloud.resources import users as nextcloud_user_manager


def available_organization_groups():
    """
    Returns a list of available groups assignable to users from an organization.
    """
    return ["OrgOwner", "User", "Admin"]


def delete_user(user_id, nextcloud_user_id, nextcloud_user_password):
    # Remove nextcloud user if exist
    if nextcloud_user_manager.user_exists_in_nextcloud(
        user_id, nextcloud_user_id, nextcloud_user_password
    ):
        nextcloud_user_manager.delete_user(
            user_id=user_id,
            auth_user_id=nextcloud_user_id,
            auth_user_app_password=nextcloud_user_password,
        )
