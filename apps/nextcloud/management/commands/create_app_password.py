import re
import subprocess
from django.core.management.base import BaseCommand, CommandError
from apps.nextcloud.occ import create_app_password, AppPasswordException


class Command(BaseCommand):
    help = "Creates an app password via http to a Nextcloud instance"

    def add_arguments(self, parser):
        parser.add_argument(
            "nc_username", type=str, help="The username of a nextcloud account"
        )
        parser.add_argument(
            "nc_password", type=str, help="The password of a nextcloud account"
        )

    def handle(self, *args, **options):
        nc_username = options["nc_username"]
        nc_password = options["nc_password"]

        return create_app_password(nc_username, nc_password)
