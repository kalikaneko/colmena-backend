from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy


class ValidatorOnlyLettersAndSpaces(RegexValidator):
    regex = r"^[a-zA-Zá-üÁ-ÜñÑ\s]+$"
    message = gettext_lazy("The field must contains letters and spaces")
