from .base import *

# ------------------------------------------------------------------------------
# Application Settings
#
DEBUG = True
DJANGO_SETTINGS_MODULE = "colmena.settings.test"
USE_COVERAGE = os.environ.get("USE_COVERAGE", "0") == "1"


# ------------------------------------------------------------------------------
# Security Settings
#
SECRET_KEY = "colmena-test-secret-key"
CORS_ORIGIN_ALLOW_ALL = True

# ------------------------------------------------------------------------------
# Apps
#
INSTALLED_APPS = INSTALLED_APPS + ["django_extensions", "django_nose"]

# ------------------------------------------------------------------------------
# Database Settings
#
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(os.path.dirname(BASE_DIR), "data/db.sqlite3"),
    }
}

# ------------------------------------------------------------------------------
# Email Settings
#
EMAIL_BACKEND = "django.core.mail.backends.dummy.EmailBackend"

# ------------------------------------------------------------------------------
# Tests
#

# nose has not catched up with python 3.11, so monkeypatching collections.Callable
# module reorganization:
if USE_COVERAGE:
    import collections

    collections.Callable = collections.abc.Callable

    TEST_RUNNER = "django_nose.NoseTestSuiteRunner"

    NOSE_ARGS = [
        "--with-coverage",
        "--cover-package=apps,colmena",
    ]
