from rest_framework import serializers
from dj_rest_auth.serializers import LoginSerializer, UserDetailsSerializer
from django.contrib.auth import get_user_model
from rest_framework import exceptions
from apps.organizations.models import UserOrganizationGroupProxy
from apps.organizations.serializers import OrganizationSerializer
from django.contrib.auth.models import Group
from drf_spectacular.utils import extend_schema_field

UserModel = get_user_model()


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ("id", "name")


class ErrorSerializer(serializers.Serializer):
    error_code = serializers.CharField(required=True, max_length=128)

    def __init__(self, error, *args, **kwargs):
        data = {"error_code": error}
        super().__init__(data, *args, **kwargs)


class ColmenaLoginSerializer(LoginSerializer):
    def validate(self, attrs):
        username = attrs.get("username")
        email = attrs.get("email")
        password = attrs.get("password")
        user = self.get_auth_user(username, email, password)
        if not user:
            msg = ErrorSerializer("ERRORS_WRONG_PASSWORD").data
            raise exceptions.AuthenticationFailed(msg)

        self.validate_auth_user_status(user)

        if not self.is_valid_user(user):
            msg = ErrorSerializer("ERRORS_USER_NOT_FOUND").data
            raise exceptions.AuthenticationFailed(msg)

        attrs["user"] = user
        return attrs

    def get_auth_user_using_orm(self, username, email, password):
        if email:
            try:
                username = UserModel.objects.get(email__iexact=email).get_username()
            except UserModel.DoesNotExist:
                msg = ErrorSerializer("ERRORS_EMAIL_NOT_FOUND").data
                raise exceptions.AuthenticationFailed(msg)

        if username:
            try:
                UserModel.objects.get(username=username).get_username()
            except UserModel.DoesNotExist:
                msg = ErrorSerializer("ERRORS_USERNAME_NOT_FOUND").data
                raise exceptions.AuthenticationFailed(msg)

            return self._validate_username_email(username, "", password)

        return None

    def is_valid_user(self, user):
        return (
            user.belongs_to_group("OrgOwner")
            or user.belongs_to_group("Admin")
            or user.belongs_to_group("User")
        )


class ColmenaUserDetailsSerializer(UserDetailsSerializer):
    organization = serializers.SerializerMethodField()
    group = serializers.SerializerMethodField()

    class Meta:
        model = UserModel
        fields = ("pk", "username", "full_name", "email", "group", "organization")

    @extend_schema_field(field=OrganizationSerializer)
    def get_organization(self, obj):
        organization = UserOrganizationGroupProxy.objects.filter(user=obj.pk)
        if organization.exists():
            return OrganizationSerializer(organization.first().organization).data
        else:
            return None

    @extend_schema_field(field=GroupSerializer)
    def get_group(self, obj):
        group = obj.groups.first()
        if group:
            return GroupSerializer(group).data
        else:
            return None
