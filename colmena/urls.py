"""
URL configuration for colmena project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from apps.accounts import views as accountViews
from apps.organizations import views as organizationViews
from django.apps import apps
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import include, re_path, path
from django.views.generic import RedirectView
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

# ------------------------------------------------------------------------------
# Routers provide an easy way of automatically determining the URL conf
#
ROUTER = routers.DefaultRouter()
ROUTER.register(r"groups", accountViews.GroupViewSet)
ROUTER.register(r"users", accountViews.UserViewSet)
ROUTER.register(r"organizations", organizationViews.OrganizationViewSet)
ROUTER.register(r"languages", accountViews.LanguageViewSet)
ROUTER.register(r"teams", organizationViews.TeamViewSet)

# ------------------------------------------------------------------------------
# Admin URLs
#
urlpatterns = i18n_patterns(
    re_path(r"^$", RedirectView.as_view(url="/admin/")),
    path("admin/", admin.site.urls),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# ------------------------------------------------------------------------------
# Account URLs
#
urlpatterns = urlpatterns + i18n_patterns(
    path(
        "<int:pk>-<token>/",
        view=accountViews.UserInvitationUpdateView.as_view(),
        name="invitations_register",
    )
)

# ------------------------------------------------------------------------------
# Router URLs
#
urlpatterns = urlpatterns + [
    re_path(r"^api/", include(ROUTER.urls)),
    re_path(r"^api/", include("apps.nextcloud.urls")),
]


# ------------------------------------------------------------------------------
# Auth URLs
#
urlpatterns = urlpatterns + [
    path(
        "api/auth/password/reset/",
        view=accountViews.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "api/auth/password/reset/confirm/",
        view=accountViews.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path("api/auth/", include("dj_rest_auth.urls")),
    path(
        "password/reset/",
        view=accountViews.AdminPasswordResetView.as_view(),
        name="admin_password_reset",
    ),
    path(
        "password/reset/confirm/<str:uid>/<str:token>/",
        view=accountViews.AdminPasswordResetConfirmView.as_view(),
        name="admin_password_reset_confirm",
    ),
]

# ------------------------------------------------------------------------------
# Invitation API
#
urlpatterns = urlpatterns + [
    path(
        "api/invitations/<int:pk>-<token>",
        view=accountViews.ConfirmInvitationView().as_view(),
        name="invitations",
    ),
    path(
        "api/invitations/organizationmember/",
        view=accountViews.OrganizationMemberViewSet().as_view(),
        name="organization_member_invitations",
    ),
]

# ------------------------------------------------------------------------------
# Countries API
#
urlpatterns = urlpatterns + [
    path(
        "api/countries",
        view=organizationViews.CountryViewSet.as_view({"get": "list"}),
        name="countries",
    ),
]


# ------------------------------------------------------------------------------
# OpenAPI schema URLs
#
urlpatterns = urlpatterns + [
    # YOUR PATTERNS
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    # Optional UI:
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
]


if settings.DEBUG:
    from django.views.defaults import page_not_found

    urlpatterns += i18n_patterns(
        re_path(r"^404/$", page_not_found),
    )

# Only enable debug toolbar if it's an installed app
if apps.is_installed("debug_toolbar"):
    import debug_toolbar

    urlpatterns += [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ]
